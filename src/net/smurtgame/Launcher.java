package net.smurtgame;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import net.smurtgame.gui.GUIPanelFactory;


/**
 * Der Launcher beinhaltet die Main-Methode, startet also das Spiel.
 * Beinhaltet ebenfalls die wichtigen Methoden run() und tick(). 
 * Initialisiert die Klasse Game.
 */
public class Launcher extends Canvas implements Runnable, MouseListener,KeyListener{


	//Public Vairablen, welche die Gr�sse und Eigenschaften des GUI's definieren. 
	public static final int WIDTH = 500;
	public static final int HEIGHT = 320;
	public static final int SCALE = 2;
	public static int DRAWABLE_HEIGHT;
	public static int DRAWABLE_WIDTH;
	
	public static final String NAME = "Project SMURT";
	public boolean running = false;
	private JFrame frame;
	private BufferedImage image = new BufferedImage(WIDTH,HEIGHT,BufferedImage.TYPE_INT_ARGB);
	
	//private int[] pixels = ((DataBufferInt)image.getRaster().getDataBuffer()).getData();
	public int tickCount;
	public long renderCount;
	
	private Game game;
	
	/*public InputHandler inputHandler;
	
	private ArrayList<Screen> screens = new ArrayList<Screen>();*/
	
	
	//Main-Methode. Startet den Launcher.
	public static void main(String[] args) {
		
		new Launcher().start();
	}
	
	//Wird von der Main Methode gestartet.
	//�ffnet das GUI, setzt gr�sse und Verhalten.
	//F�gt Mouselistener hinzu
	public Launcher(){
		try {
			try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (ClassNotFoundException | InstantiationException
					| IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Dimension sizes = new Dimension(WIDTH * SCALE, HEIGHT * SCALE);
		setMinimumSize(sizes);
		setMaximumSize(sizes);
		setPreferredSize(sizes);
		frame = new JFrame();
		frame.setIconImage(ImageLoader.loadImage("/artwork/Icon.png"));
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setTitle(NAME);
		frame.setLayout(new BorderLayout());
		frame.add(this, BorderLayout.CENTER);
		
		
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		
		
		JPanel gamePanel = new JPanel();
		gamePanel.setPreferredSize(sizes);
		
		this.DRAWABLE_HEIGHT = frame.getContentPane().getHeight();
		this.DRAWABLE_WIDTH = frame.getContentPane().getWidth();
		
		game = new Game();
		frame.add(gamePanel);
		

		this.addMouseListener(game);
		this.addMouseMotionListener(game);
		this.addKeyListener(game);
		
		ImageLoader.debug();
		
	}

	//L�uft w�hrend des ganzen Spiels. 
	//Z�hlt anzahl ticks und rendert das Spiel regelm�ssig.
	public void run() {
		
		long lastTime = System.nanoTime();
		double nsPerTick = 1000000000D/60D;
		int fps = 0;
		int ticks = 0;
		int frames = 0;
		long lastTimer = System.currentTimeMillis();
		double delta = 0;
		
		
		while(running){
			long now = System.nanoTime();
			delta += (now - lastTime) / nsPerTick;
			lastTime = now;
			boolean shouldRender = false;
			
			while(delta >= 1){
				ticks++;
				tick();
				delta--;
				shouldRender = true;
			}
			
			
			
			
			//tick();
			if(shouldRender){
				frames++;
				render();
				
				
			}
			
			
			if(System.currentTimeMillis() - lastTimer >= 1000){
				//System.out.println(frames +", "+ticks);
				lastTimer += 1000;
				frames = 0;
				ticks = 0;
			}
			
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	//F�hrt Tick Methode in Game aus.
	public void tick(){
		
		/*for(Screen s : screens){
			s.tick();
		}*/
		tickCount++;
		game.tick();
		
		
		
	}
	
	//Wird regelm�ssig von der run methode ausgef�hrt. 
	//Aktualisiert die gerenderten Objekte.
	public void render(){
		BufferStrategy bs = getBufferStrategy();
		if(bs == null){
			createBufferStrategy(2);
			return;
		}
		Graphics2D  g = (Graphics2D)bs.getDrawGraphics();
		
		g.setColor(Color.BLACK);
		
		g.fillRect(0, 0, getWidth(),getHeight());
		g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
		
		/*for(Screen s : screens){
			s.render(g);
		}*/
		
		
		
		game.render(g);
		g.dispose();
		bs.show();
		
		
		
		renderCount++;
	}
	
	private synchronized void start(){
		running = true;
		new Thread(this).start();
	}
	
	private synchronized void stop(){
		running = false;
	}

	//Listeners (Geben zu Debug-Zwecken alle ihre aktion aus)
	@Override
	public void mouseClicked(MouseEvent e) {
		System.out.println("Launcher "+e.getX() + "; "+e.getY());
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		System.out.println("mouseEntered");
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		System.out.println("mouseExited");
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		System.out.println("mousePressed");
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		System.out.println("mouseReleased");
		
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		System.out.println("keyPressed");
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
