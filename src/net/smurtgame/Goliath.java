package net.smurtgame;

import java.awt.Image;
import java.awt.Rectangle;

/**
 * Diese Klasse beinhaltet den Panzertyp Goliath(Schwerer Panzer)
 * Der Goliath besitzt eine kleine Reichweite und kann sich schlecht bewegen. Jedoch hat er eine starke Feuerkraft. 
 * Superklasse dieser Klasse ist Panzer.
 */
public class Goliath extends Panzer{
	
	// heavy tank, high strengh/low movement
	public Goliath (Image img, Rectangle pos, int z){
		// defines the properties of the tank
		super(img,pos, z,100,35,3,4);
		resetMovePoints();
		
	}
	@Override
	public void resetMovePoints(){
		this.setMovePoints(1);
	}

}
