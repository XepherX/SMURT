package net.smurtgame.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import net.smurtgame.gui.PlayerFoundListener;
import net.smurtgame.network.packet.DiscoveryPlayerlist;
import net.smurtgame.network.packet.GameStartPacket;
import net.smurtgame.network.packet.GameTankposPacket;
import net.smurtgame.network.packet.GameTilearray;
import net.smurtgame.network.packet.Packet;

/**
 * 
 * @author Fabian
 * Der Netzwerkclient des Spieles.
 */
public class NetworkClient extends Thread{

	public InetAddress ipAddress; // Die IP, auf die der client verbunden ist.
	private DatagramSocket socket;
	private ArrayList<PacketReceivedListener> listeners = new ArrayList<PacketReceivedListener>();
	private ArrayList<PlayerFoundListener> listenersPlayer = new ArrayList<PlayerFoundListener>();
	private ArrayList<RemotePlayer> connected = new ArrayList<RemotePlayer>();
	private ArrayList<Remoteable> tankListeners = new ArrayList<Remoteable>();
	
	/**
	 * Defaultkonstruktor.
	 * @param ipAddress - Die IP, auf die der Client sich verbinden soll.
	 */
	public NetworkClient(String ipAddress){
		try {
			this.socket = new DatagramSocket();
			this.ipAddress = InetAddress.getByName(ipAddress);
		} catch (SocketException | UnknownHostException e) {

			e.printStackTrace();
		}
	}
	
	/**
	 * Wird durch Thread.start aufgerufen.
	 * Das ist das Kernst�ck des Netzwerkes, auf clientseite. Ein Packet wird hier versucht zu empfangen.
	 * Das wird die Antwort des Servers auf eine vorherige Anfrage sein. Anschliessend wird nach PacketID 
	 * entsprechende aktionen ausgel�st.
	 */
	public void run(){
		while(true){
			byte[] data = new byte[4096];
			DatagramPacket packet = new DatagramPacket(data,data.length);
			try {
				socket.receive(packet);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String message = new String(packet.getData()).trim();
			Packet rec = new Packet(message, packet.getAddress());
			
			
			
			for(PacketReceivedListener listener : listeners){
				listener.onPacketReceived(rec);
				
			}
			
			if(rec.getPacketId() == Packet.PACKET_PLAYERLIST_PREGAME){ // Eine Liste von verbundenen Spielern wurde gesandt.
				for(PlayerFoundListener listener : listenersPlayer){
					/*DiscoveryPacket recPack = DiscoveryPacket.fromString(rec.getRawMessage(), rec.getSender());
					RemotePlayer oponent = new RemotePlayer(recPack.getSender(),recPack.getPlayerName());
					listener.onPlayerFound(oponent);*/
					
					DiscoveryPlayerlist playerlist = DiscoveryPlayerlist.fromString(rec.getRawMessage(), rec.getSender());
					connected = playerlist.getRemotePlayers();
					
					listener.onPlayerlistReceived(connected);
				}
				
				
					
			}
			
			if(rec.getPacketId() == Packet.PACKET_TANK_NEWPOS){ // Ein Panzer wurde bewegt; das ist die neue Position.
				System.out.println("tank update received.");
				for(Remoteable r : tankListeners)
				{
					r.onTankPosUpdated(GameTankposPacket.fromString(rec.getRawMessage(), rec.getSender()));
				}
			}
			
			
			if(rec.getPacketId() == Packet.PACKET_GAME_START) // Ein Spiel wurde initiiert.
			{
				GameStartPacket pack = GameStartPacket.fromString(rec.getRawMessage(), rec.getSender());
				System.out.println("connecting. lets commence the fun.");
				
				InetSocketAddress remoteAddr = new InetSocketAddress(packet.getAddress(), packet.getPort());
				
				RemotePlayer remote = new RemotePlayer(remoteAddr, pack.getPlayerName());
				
				for(PlayerFoundListener listener : this.listenersPlayer)
				{
					listener.onRemoteplayerStarted(remote);
				}
					
				
			}
				
			if(rec.getPacketId() == Packet.PACKET_GAME_TILES)
			{
				/*System.out.println("received tiles from server.");
				GameTilearray.fromString(rec.getRawMessage(),rec.getSender());*/
				
			}
				
			
		}
	}
	/**
	 * Sendet ein byte[] �ber das Netzwerk
	 * @param data - Byte[] das verschickt werden soll.
	 */
	public void sendData(byte[] data){
		DatagramPacket packet = new DatagramPacket(data,data.length,ipAddress,9001);
		try {
			socket.send(packet);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Ein Wrapper f�r SendData. 
	 * @param p Das Packet, das versandt werden soll.
	 */
	public void sendPacket(Packet p){
		sendData(p.getData());
	}
	
	/**
	 * F�gt einen PacketReceivedListener hinzu.
	 * @param listener - PacketReceivedListener.
	 */
	public void addOnPacketReceivedListener(PacketReceivedListener listener)
	{
		listeners.add(listener);
	}
	
	/**
	 * F�gt einen PlayerFoundListener hinzu.
	 * @param listener - PlayerFoundListener.
	 */
	public void addOnPlayerFoundListener(PlayerFoundListener listener)
	{
		listenersPlayer.add(listener);
	}
	
	/**
	 * Wird verwendet, um direkt ein UDP-Packet zu versenden, ohne auf Wrapper zuzugreiffen.
	 * @param dp DatagramPacket, das verschickt wird.
	 */
	public void sendDatagram(DatagramPacket dp)
	{
		try {
			this.socket.send(dp);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Gibt eine Liste mit verf�gbaren Clients zur�ck.
	 * @return ArrayList<RemotePlayer> - Verf�gbare Clients.
	 */
	public ArrayList<RemotePlayer> connectedPlayers() {
		return this.connected;
	}
	
	/**
	 * F�gt einen RemoteableListener hinzu.
	 * @param r Remoteable, das hinzugef�gt wird.
	 */
	public void addRemoteableListener(Remoteable r)
	{
		this.tankListeners.add(r);
	}


	
}
