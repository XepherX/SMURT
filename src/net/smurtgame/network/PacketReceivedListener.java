package net.smurtgame.network;

import net.smurtgame.network.packet.Packet;

/**
 * Interface f�r Objekte, die �ber eintreffende Packete benachrichtigt werden sollen.
 * @author Fabian
 *
 */
public interface PacketReceivedListener {
	/**
	 * 
	 * @param packet Das eingetroffene Packet.
	 */
	public void onPacketReceived(Packet packet);
}
