package net.smurtgame.network.packet;

import java.awt.Point;
import java.net.InetAddress;

import net.smurtgame.Panzer;

/**
 * Bidirektionales Panzer positions update. 
 * @author Fabian
 *
 */
public class GameTankposPacket extends Packet {

	private Point newPos;
	private int tankid;
	
	/**
	 * Konstruktor f�r das sendende Ende, der Panzer wird �bergeben und daraus das Update errechnet.
	 * @param panzer
	 * @param sender
	 */
	public GameTankposPacket(Panzer panzer, InetAddress sender) {
		super(Packet.PACKET_TANK_NEWPOS+";", sender);
		
		if(panzer.getNewPos() == null)
			this.setRawMessage(Packet.PACKET_TANK_NEWPOS+";"+panzer.getId()+";"+panzer.getPos().x+";"+panzer.getPos().y);
		else
			this.setRawMessage(Packet.PACKET_TANK_NEWPOS+";"+panzer.getId()+";"+panzer.getNewPos().x+";"+panzer.getNewPos().y);
		
	}
	
	/**
	 * Alternativer Konstruktor, damit ohne Panzer ein Update generiert werden kann. Ist beim empfangenden Ende der Fall.
	 * @param id
	 * @param newpoint
	 * @param sender
	 */
	public GameTankposPacket(int id, Point newpoint, InetAddress sender) {
		super(Packet.PACKET_TANK_NEWPOS+";", sender);
		
		this.setRawMessage(Packet.PACKET_TANK_NEWPOS+";"+id+";"+newpoint.x+";"+newpoint.y);
		this.newPos = newpoint;
		this.tankid = id;
	}
	
	/**
	 * getter f�r TankId
	 * @return
	 */
	
	public int getTankId(){
		return this.tankid;
	}
	
	/**
	 * getter f�r newPos
	 * @return
	 */
	public Point getNewPos(){
		return this.newPos;
	}
	
	/**
	 * Statische Methode, um ein Packet aus einer rawMessage zu erstellen, da Casting nicht funktioniert.
	 * @param rawMessage
	 * @param sender
	 * @return
	 */
	public static GameTankposPacket fromString(String rawMessage, InetAddress sender)
	{
		int id, x, y;
		
		String [] fields = rawMessage.split(";");
		id = Integer.parseInt(fields[1]);
		x = Integer.parseInt(fields[2]);
		y = Integer.parseInt(fields[3]);
		
		Point point = new Point(x,y);
		GameTankposPacket ret = new GameTankposPacket(id, point, sender);
		return ret;
	}
	

}
