package net.smurtgame.network.packet;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;

import net.smurtgame.gui.Tile;




public class GameTilearray extends Packet {
	private Tile[][] tiles;
	
	public GameTilearray(Tile[][] tiles, InetAddress sender) {
		super(Packet.PACKET_GAME_TILES+";", sender);
		String tilesSerialized = ""; 
		String comma = "";
		
		System.out.println("serializing "+(tiles.length * tiles[0].length)+" tiles.");
		
		for(int i = 0; i < tiles.length; i++)
		{
			for(int j = 0; j < tiles[i].length; j++){
				tilesSerialized += comma + tiles[i][j].getTileID();
				comma = ";";
			}
			comma = ";";
			
		}
		
		System.out.println("the array looks like this: "+Arrays.deepToString(tiles));
		
		this.setRawMessage(Packet.PACKET_GAME_TILES+";"+tilesSerialized);
		this.tiles = tiles;
		
	}

	public static GameTilearray fromString(String rawMessage, InetAddress sender) {
		
		
		Tile[][] tiles = new Tile[19][30];
		int[][] tmp = new int[19][30];
		String[] fields = rawMessage.split(";");
		
		
		System.out.println("raw fields: "+Arrays.deepToString(fields));
		System.out.println("raw fields length: "+fields.length+", need "+19*30);
		
		
		for(int y = 0 ; y < tiles.length; y++)
		{
			for(int x = 0; x < tiles[y].length; x++)
			{
				int calcIndex = (y * tiles[y].length) + x + 1;
				System.out.println(calcIndex);
				
				tiles[y][x] = Tile.fromID(Integer.parseInt(fields[calcIndex]));
				tmp[y][x] = Integer.parseInt(fields[calcIndex]);
			}
		}
		
		System.out.println("the array looks like this: "+Arrays.deepToString(tmp));
		
		return new GameTilearray(tiles,sender);
		
		
	}
	
	public Tile[][] getTilearray()
	{
		return this.tiles;
	}

}
