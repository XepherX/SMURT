package net.smurtgame.network.packet;

import java.net.InetAddress;


/**
 * Der Kern des Netzwerkes. H�lt die IP und die rohe nachricht aus dem netz.
 * @author Fabian
 *
 */
public class Packet {
	
	private String rawMessage;
	private InetAddress sender;
	private int packetId;
	
	public static int PACKET_REQUESTSERVER = 0;
	public static int PACKET_SERVERANSWER = 1;
	public static int PACKET_PLAYERLIST_PREGAME = 2;
	public static int PACKET_GAME_START = 3;
	public static int PACKET_TANK_NEWPOS = 4;
	public static int PACKET_GAME_TILES = 5;
	public static int PACKET_GAME_HEALTH = 6;
	public static int PACKET_GAME_TURNSYNC = 7;
	
	
	/**
	 * getter f�r rawMessage
	 * @return
	 */
	public String getRawMessage() {
		return rawMessage;
	}
	
	/**
	 * getter f�r packetId
	 * @return
	 */
	public int getPacketId(){
		return this.packetId;
	}

	/**
	 * setter f�r rawMessage
	 * @param rawMessage
	 */
	public void setRawMessage(String rawMessage) {
		this.rawMessage = rawMessage;
	}

	/**
	 * getter f�r sender
	 * @return
	 */
	public InetAddress getSender() {
		return sender;
	}

	/**
	 * setter f�r sender
	 * @param sender
	 */
	public void setSender(InetAddress sender) {
		this.sender = sender;
	}

	/**
	 * Konstruktor f�r das Packet. 
	 * @param message die rohe Nachricht aus dem Netz.
	 * @param sender sender
	 */
	public Packet(String message, InetAddress sender){
		this.rawMessage = message;
		this.sender = sender;
		
		this.packetId = Integer.parseInt(message.split(";")[0]);
	}

	/**
	 * transformiert die nachricht in bytes, damit sie �ber das Netzwerk verschickt werden k�nnen.
	 * @return
	 */
	public byte[] getData() {
		
		return new String(rawMessage).getBytes();
	}
	

}
