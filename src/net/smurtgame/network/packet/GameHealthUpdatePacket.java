package net.smurtgame.network.packet;

import java.net.InetAddress;

import net.smurtgame.Panzer;

public class GameHealthUpdatePacket extends Packet {

	private int TankId;
	private int newHealth;
	
	
	
	public int getTankId() {
		return TankId;
	}

	public void setTankId(int tankId) {
		TankId = tankId;
	}

	public int getNewHealth() {
		return newHealth;
	}

	public void setNewHealth(int newHealth) {
		this.newHealth = newHealth;
	}

	public GameHealthUpdatePacket(Panzer p, InetAddress sender) {
		super(Packet.PACKET_GAME_HEALTH+";", sender);
		super.setRawMessage(Packet.PACKET_GAME_HEALTH+";"+p.getId()+";"+p.getHealth());
		

	}
	
	public GameHealthUpdatePacket(int TankId, int newHealth, InetAddress sender)
	{
		super(Packet.PACKET_GAME_HEALTH+";", sender);
		this.TankId = TankId;
		this.newHealth = newHealth;
		setRawMessage(Packet.PACKET_GAME_HEALTH+";"+TankId+";"+newHealth);
		
	}
	
	public static GameHealthUpdatePacket fromString(String rawMessage, InetAddress sender)
	{
		String[] fields = rawMessage.split(";");
		int id = Integer.parseInt(fields[1]);
		int h = Integer.parseInt(fields[2]);
		return new GameHealthUpdatePacket(id,h,sender);
	}
	
	

}
