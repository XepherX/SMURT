package net.smurtgame.network.packet;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import net.smurtgame.network.RemotePlayer;

/**
 * Der Server meldet alle verbundenen Spieler zur�ck an den Client.
 * @author Fabian
 *
 */
public class DiscoveryPlayerlist extends Packet {

	private ArrayList<RemotePlayer> players = new ArrayList<RemotePlayer>();
	/**
	 * Die verbundenen Spieler werden zu einem String serialisiert und versandt.
	 * @param players
	 * @param sender
	 */
	public DiscoveryPlayerlist(ArrayList<RemotePlayer> players, InetAddress sender) {
		super(Packet.PACKET_PLAYERLIST_PREGAME+";", sender);
		
		String playerString = "";
		for(RemotePlayer player : players)
			playerString += player.getPlayerName()+";"+player.getSocketAddress().toString().replace("/", "")+";";
		
		playerString = players.size() + ";" + playerString;
		
		this.setRawMessage(Packet.PACKET_PLAYERLIST_PREGAME+";"+playerString);
		
		System.out.println("packet "+playerString);
		this.players = players;
		
		

	}

	/**
	 * Getter f�r die verbundenen Spieler.
	 * @return
	 */
	public ArrayList<RemotePlayer> getRemotePlayers(){
		return this.players;
	}
	
	/**
	 * Statische Methode, um ein Packet aus einer rawMessage zu erstellen, da Casting nicht funktioniert.
	 * @param rawMessage
	 * @param sender
	 * @return
	 */
	public static DiscoveryPlayerlist fromString(String rawMessage, InetAddress sender) {
		
		DiscoveryPlayerlist ret = null;
		
		ArrayList<RemotePlayer> players = new ArrayList<RemotePlayer>();
		String[] fields = rawMessage.split(";");
		
		int playerCount = Integer.parseInt(fields[1]);
		
		System.out.println("received "+playerCount+" players.");
		
		for(int i = 0; i < playerCount; i++){
			String playerName = fields[i*2+2];
			String playerIp = fields[i*2+3];
			
			
			InetSocketAddress s = new InetSocketAddress(playerIp.split(":")[0],Integer.parseInt(playerIp.split(":")[1]));
			
			players.add(new RemotePlayer(s,playerName));
			
		}
		
		
		//ret = new DiscoveryPlayerlist()
		

		return new DiscoveryPlayerlist(players,sender);
	}

}
