package net.smurtgame.network.packet;

import java.net.InetAddress;

/**
 * Der Client sucht einen Server
 * @author Fabian
 *
 */
public class DiscoveryPacket extends Packet {
	
	private String playerName;
	
	/**
	 * Der Client sagt seinen Namen und gibt automatisch seine IP mit.
	 * @param playerName Der Name des Spielers
	 * @param sender Seine IP
	 */
	public DiscoveryPacket(String playerName, InetAddress sender) {
		super(Packet.PACKET_REQUESTSERVER + ";"+playerName,sender);
		this.playerName = playerName;
	}
	
	/**
	 * Getter f�r Playername
	 * @return
	 */
	public String getPlayerName(){
		return playerName;
	}
	
	/**
	 * Statische Methode, um ein Packet aus einer rawMessage zu erstellen, da Casting nicht funktioniert.
	 * @param rawMessage Die RawMessage des Packets.
	 * @param sender Die IP des Packets
	 * @return
	 */
	public static DiscoveryPacket fromString(String rawMessage, InetAddress sender)
	{
		String pname = rawMessage.split(";")[1];
		return new DiscoveryPacket(pname, sender);
	}
}
