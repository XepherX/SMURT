package net.smurtgame.network.packet;

import java.net.InetAddress;

/**
 * Entweder der Client oder der Server initiert das Spiel.
 * @author Fabian
 *
 */
public class GameStartPacket extends Packet {

	private String playerName;
	
	/**
	 *  Der Name und die IP des initiierers werden �bermittelt.
	 * @param playerName
	 * @param sender
	 */
	public GameStartPacket(String playerName, InetAddress sender) {
		super(Packet.PACKET_GAME_START + ";"+playerName,sender);
		this.playerName = playerName;
		super.setRawMessage(Packet.PACKET_GAME_START+";"+playerName);
	}
	
	/**
	 * getter f�r playername
	 * @return
	 */
	public String getPlayerName(){
		return playerName;
	}
	
	/**
	 * Statische Methode, um ein Packet aus einer rawMessage zu erstellen, da Casting nicht funktioniert.
	 * @param rawMessage
	 * @param sender
	 * @return
	 */
	public static GameStartPacket fromString(String rawMessage, InetAddress sender)
	{
		System.out.println("created gsp from string with "+rawMessage);
		String pname = rawMessage.split(";")[1];
		return new GameStartPacket(pname, sender);
	}
}
