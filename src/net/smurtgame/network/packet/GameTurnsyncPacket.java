package net.smurtgame.network.packet;

import java.net.InetAddress;

public class GameTurnsyncPacket extends Packet {

	private boolean turnSync;
	
	
	public boolean getTurnSync() {
		return turnSync;
	}


	public void setTurnSync(boolean turnSync) {
		this.turnSync = turnSync;
	}


	public GameTurnsyncPacket(boolean isServerTurn, InetAddress sender) {
		super(Packet.PACKET_GAME_TURNSYNC+";"+(isServerTurn ? "1" : "0"), sender);
		turnSync = isServerTurn;
		

	}
	
	
	public static GameTurnsyncPacket fromString(String rawMessage, InetAddress sender) {
		
		String[] fields = rawMessage.split(";");
		System.out.println("raw packet: "+rawMessage);
		System.out.println("parsed: "+fields[1].equals("1"));
		System.out.println("field: "+fields[1]);
		return new GameTurnsyncPacket(fields[1].equals("1"),sender);
	}


}
