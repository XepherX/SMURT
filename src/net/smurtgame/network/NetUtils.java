package net.smurtgame.network;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

/**
 * Eine kleine Hilfsklasse f�r Netzwerkabfragen.
 */
public class NetUtils {
	/**
	 * 	
	 * @param addr Die InetAddress, die gepr�ft werden soll.
	 * @return boolean - Ist die InetAddress gleich des LocalHost?
	 */
	public static boolean isMe(InetAddress addr)
	{
		try {
			return addr.getHostAddress().equals(InetAddress.getLocalHost().getHostAddress());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}

	public static InetAddress getMe()
	{
		try {
			return InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
}
