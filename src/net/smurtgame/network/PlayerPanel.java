package net.smurtgame.network;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;

import net.smurtgame.Launcher;
import net.smurtgame.gui.ImageElement;

/**
 * GuiElement, welches einen Spieler beinh�lt.
 * @author Fabian
 *
 */
public class PlayerPanel extends ImageElement {

	
	
	
	
	private FontMetrics metrics;
	private RemotePlayer player;
	private Font f = new Font("Arial",Font.PLAIN, 30);
	
	
	public PlayerPanel(Rectangle rect, int zIndex, Image img, int scale) {
		super(rect, zIndex, img, scale);
		

	}
	public void setPlayer(RemotePlayer player)
	{
		this.player = player;
	}
	
	public RemotePlayer getPlayer(){
		return this.player;
	}
	
	

	@Override
	public void render(Graphics2D g) {
		super.render(g);
		if(metrics == null)
			metrics = g.getFontMetrics(f);
		
		int width = metrics.stringWidth(player.getPlayerName());
		g.setFont(f);
		g.setColor(Color.white);
		g.drawString(player.getPlayerName(), Launcher.DRAWABLE_WIDTH / 2 - width / 2, 50 + super.getPos().y);
		
		
	}
	
	

}
