package net.smurtgame.network;

import net.smurtgame.network.packet.GameTankposPacket;
import net.smurtgame.network.packet.Packet;

/**
 * Interface f�r alle Objekte, die �ber Netzwerk versendet werden k�nnen.
 * @author Fabian
 *
 */
public interface Remoteable {
	/**
	 * Serialisiert das Objekt in ein Packet.
	 * @return
	 */
	public Packet toPacket();
	/**
	 * Informiert einen Panzer, dass er vom gegner bewegt worden ist.
	 * @param packet
	 */
	public void onTankPosUpdated(GameTankposPacket packet);

}
