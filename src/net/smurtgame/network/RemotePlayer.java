package net.smurtgame.network;

import java.net.InetSocketAddress;
import java.net.SocketAddress;

import net.smurtgame.Player;
import net.smurtgame.network.packet.Packet;

/**
 * Stellt den Gegner dar.
 * @author Fabian
 *
 */
public class RemotePlayer extends Player{
	
	private InetSocketAddress sockAddr;
	private String playerName;
	

	/**
	 * Standardkonstruktor mit SocketAddress und Spielername.
	 * @param sockAddr
	 * @param playerName
	 */
	public RemotePlayer(InetSocketAddress sockAddr, String playerName)
	{
		this.sockAddr = sockAddr;
		this.playerName = playerName;
		System.out.println("set the name to "+playerName);
	}


	/**
	 * Getter f�r Spielername.
	 * @return
	 */
	public String getPlayerName() {
		return this.playerName;
	}

	/**
	 * Setter f�r Spielername.
	 * @param playerName
	 */
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	
	/**
	 * Getter f�r SocketAddress.
	 * @return
	 */
	public InetSocketAddress getSocketAddress(){
		return this.sockAddr;
	}
	
	
	
	
	
	

}
