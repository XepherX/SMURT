package net.smurtgame.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import net.smurtgame.gui.PlayerFoundListener;
import net.smurtgame.network.packet.DiscoveryPacket;
import net.smurtgame.network.packet.DiscoveryPlayerlist;
import net.smurtgame.network.packet.GameStartPacket;
import net.smurtgame.network.packet.GameTankposPacket;
import net.smurtgame.network.packet.Packet;

/**
 * Dies ist der Netzwerkserver, der clients �ber ver�nderungen informiert.
 * @author Fabian
 *
 */
public class NetworkServer extends Thread{
	
	
	private DatagramSocket socket;
	private ArrayList<PacketReceivedListener> listeners = new ArrayList<PacketReceivedListener>();
	private ArrayList<PlayerFoundListener> listenersPlayer = new ArrayList<PlayerFoundListener>();
	private ArrayList<RemotePlayer> connected = new ArrayList<RemotePlayer>();
	private ArrayList<Remoteable> tankListeners = new ArrayList<Remoteable>();
	
	/**
	 * Standardkonstruktor f�r den Server.
	 */
	public NetworkServer(){
		try {
			this.socket = new DatagramSocket(9001);
			
		} catch (SocketException e) {

			e.printStackTrace();
		}
	}
	/**
	 * f�gt einen PacketReceivedListener hinzu.
	 * @param listener PacketReceivedListener.
	 */
	public void addOnPacketReceivedListener(PacketReceivedListener listener)
	{
		listeners.add(listener);
	}
	
	/**
	 * f�gt einen PlayerFoundListener hinzu.
	 * @param listener PlayerFoundListener.
	 */
	public void addOnPlayerFoundListener(PlayerFoundListener listener)
	{
		System.out.println("add playerfoundlistener to server");
		listenersPlayer.add(listener);
	}
	
	/**
	 * Dies ist das Kernst�ck des Servers. Beinh�lt das Netzwerkprotokol und die Verbindungen. Funktioniert �hnlich wie der NetzwerkClient.
	 */
	public void run(){
		while(true){
			byte[] data = new byte[4096];
			DatagramPacket packet = new DatagramPacket(data,data.length);
			try {
				socket.receive(packet);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String message = new String(packet.getData());
			
			try {
				
				if(InetAddress.getLocalHost().getHostAddress().trim().equals(packet.getAddress().getHostAddress().trim())){
					System.out.println("Ignoring message from myself.");
					
				} else {
					System.out.println("CLIENT("+packet.getAddress().getHostAddress()+"): \""+message.trim()+"\"");
				}
				
				Packet received = new Packet(message.trim(),packet.getAddress());
		
				for(PacketReceivedListener listener : listeners)
				{
					listener.onPacketReceived(received);	
				}
				
				if(received.getPacketId() == Packet.PACKET_TANK_NEWPOS){
					System.out.println("tank update received.");
					for(Remoteable r : tankListeners)
					{
						r.onTankPosUpdated(GameTankposPacket.fromString(received.getRawMessage(), received.getSender()));
					}
				}
				
				if(received.getPacketId() == Packet.PACKET_REQUESTSERVER)	
				{
					DiscoveryPacket recPack = DiscoveryPacket.fromString(received.getRawMessage(), received.getSender());
					InetSocketAddress remoteAddr = new InetSocketAddress(packet.getAddress(), packet.getPort());
					RemotePlayer oponent = new RemotePlayer(remoteAddr,recPack.getPlayerName());
					connected.add(oponent);
					System.out.println(connected.size()+" players need to be updated. listeners: "+listenersPlayer.size());
					for(PlayerFoundListener listener : listenersPlayer){
						listener.onPlayerFound(oponent);
						System.out.println("notifiing about "+oponent.getPlayerName());
					}
					DiscoveryPacket castRec = DiscoveryPacket.fromString(received.getRawMessage(), received.getSender());
					System.out.println("Responding to client from server"+packet.getAddress().getHostAddress());
					
					DiscoveryPlayerlist playerList = new DiscoveryPlayerlist(this.connectedPlayers(), recPack.getSender());
					
					
					//DiscoveryAnswerPacket pack = new DiscoveryAnswerPacket(InetAddress.getLocalHost(), castRec.getPlayerName());
					
					
					// TODO: ALLE PLAYER UPDATEN DAS IST EIN BUG!!
					DatagramPacket p = null;
					try {
						p = new DatagramPacket(playerList.getData(), playerList.getData().length,packet.getSocketAddress());
						
					} catch (SocketException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					try {
						socket.send(p);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					

				}
				
								
				
				if(received.getPacketId() == Packet.PACKET_GAME_START)
				{
					GameStartPacket pack = GameStartPacket.fromString(received.getRawMessage(), received.getSender());
					
					InetSocketAddress remoteAddr = new InetSocketAddress(packet.getAddress(), packet.getPort());
					
					RemotePlayer remotePlayer = new RemotePlayer(remoteAddr, pack.getPlayerName());
					
					
					for(PlayerFoundListener listener : this.listenersPlayer)
					{
						listener.onRemoteplayerStarted(remotePlayer);
					}
						
					
				}
									
				
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
		}
	}
	
	/**
	 * Sendet daten an eine IP.
	 * @param data Die zu sendenden Daten.
	 * @param ipAddress die IP, an die Daten geschickt werden sollen.
	 * @param port Der Port, bei der der gegen�berstehende Client mith�rt.
	 */
	public void sendData(byte[] data, InetAddress ipAddress, int port){
		DatagramPacket packet = new DatagramPacket(data,data.length,ipAddress,port);
		try {
			socket.send(packet);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Verschickt ein Packet an einen Player.
	 * @param pack das Packet
	 * @param remote der Player.
	 */
	public void sendPacket(Packet pack, RemotePlayer remote)
	{
		try {
			DatagramPacket data = new DatagramPacket(pack.getData(),0,pack.getData().length,remote.getSocketAddress());
			sendDatagram(data);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Gibt alle verbundenen Spieler zur�ck.
	 * @return
	 */
	public ArrayList<RemotePlayer> connectedPlayers() {
		return this.connected;
	}
	
	/**
	 * f�gt einen Remoteable zu den panzer update listeners.
	 * @param r Remoteable
	 */
	public void addRemoteableListener(Remoteable r)
	{
		this.tankListeners.add(r);
	}

	/**
	 * Schickt einen DatagramPacket direkt, ohne wrapper.
	 * @param dp das zu versendende DatagramPacket.
	 */
	public void sendDatagram(DatagramPacket dp)
	{
		try {
			this.socket.send(dp);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
