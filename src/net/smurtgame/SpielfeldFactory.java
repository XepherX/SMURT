package net.smurtgame;

/**
 * In der Factory wird mithilfe von Random-Methoden ein Spielfeld zuf�llig generiert.
 * Die Art des Tiles wird mit Zahlen definiert: 
 * 1 Baumspitze, 2 = Busch, 4 = Wasser, 5 = Stein, 8 = Basis, 10 = Boden
 */
public class SpielfeldFactory {
	static int zufallsZahl;

		//Diese Methode generiert ein Zahlenarray, welches aus Steinen, Boden und B�schen besteht, die Spielfeldart Plains.
		public static int[][] createPlains(int hoehe, int breite){
			hoehe += 4;
			int[][] spielFeld = new int[hoehe][breite];
			
			for (int h = 0; h < hoehe; h++){
				for (int b = 0; b < breite; b++){
					
					//Basis
					if(h < 2 || h > (hoehe - 3)){
						spielFeld[h][b] = 8; //8 = Basis
					}
					
					//Feld
					else{
						zufallsZahl = ((int) (Math.random() * (30 - 1) + 1)); // Zufall 30
						
						if (zufallsZahl == 1 && spielFeld[h][b] == 0){
							spielFeld[h][b] = 5; //5 = Stein
						}
						else if (zufallsZahl == 2 && spielFeld[h][b] == 0){
							spielFeld[h][b] = 2; //2 = Busch
						}
						else if(spielFeld[h][b] == 0){
							spielFeld[h][b] = 10; //10 = Waldboden
						}
					}

				}				
			}
			return spielFeld;

			
		}
	
	
	//Diese Methode generiert ein Zahlenarray, welches aus B�umen, Boden und B�schen besteht, die Spielfeldart Wald.
	public static int[][] createForest(int hoehe, int breite){
		hoehe += 4;
		int[][] spielFeld = new int[hoehe][breite];
		
		for (int h = 0; h < hoehe; h++){
			for (int b = 0; b < breite; b++){
				
				//Basis
				if(h < 2 || h > (hoehe - 3)){
					spielFeld[h][b] = 8; //8 = Basis
				}
				
				//Feld
				else{
					zufallsZahl = ((int) (Math.random() * (30 - 1) + 1)); // Zufall 30
					
					if (zufallsZahl < 4 && spielFeld[h][b] == 0 && h < (hoehe - 4)){
						spielFeld[h][b] = 1; //1 = Baum
						spielFeld[h+1][b] = 9; //9 = Baumstumpf
					}
					else if (zufallsZahl == 4 && spielFeld[h][b] == 0){
						spielFeld[h][b] = 2; //2 = Busch
					}
					else if(spielFeld[h][b] == 0){
						spielFeld[h][b] = 10; //10 = Waldboden
					}
				}
			}			
		}
		return spielFeld;

		
	}
	
	
	//Diese Methode generiert ein Zahlenarray, welches aus Steinen, Boden und Wasser besteht, die Spielfeldart See.
	//Die Chance auf Wasser ist erh�ht, wenn mehr Wasser in der N�he ist.
	public static int[][] createLake(int hoehe, int breite){
		hoehe += 4;
		int[][] spielFeld = new int[hoehe][breite];
		
		for (int h = 0; h < hoehe; h++){
			for (int b = 0; b < breite; b++){
				
				//Basis
				if(h < 2 || h > (hoehe - 3)){
					spielFeld[h][b] = 8; //8 = Basis
				}
				
				//Feld
				else{
					//Abst�nde des Randes
					if (h < hoehe - 4 && h > 3 && b > 7 && b < breite - 8) {
						
						//Wenn oben und links NICHT Wasser, Chance auf Wasser verkleinert
						if (spielFeld[h - 1][b] != 4 && spielFeld[h][b - 1] != 4) {
							zufallsZahl = (int) Math.round(Math.random() * (150 - 1) + 1); // % auf Wasser
							
						//Wenn oben und Links Wasser, Chance auf Wasser erh�ht
						} else {
							zufallsZahl = (int) Math.round(Math.random() * (13 - 1) + 1); // % auf Wasser
						}
						
					//Wenn erst auf gewisse Entfernung zum Rand normale Chance
					} else {
						zufallsZahl = (int) Math.round(Math.random() * (150 - 1) + 1); // % auf Wasser
					}
					
					if (zufallsZahl <= 10 && spielFeld[h][b] == 0){
						spielFeld[h][b] = 4; //4 = Wasser
					}
					else if (zufallsZahl > 140 && zufallsZahl < 145 && spielFeld[h][b] == 0){
						spielFeld[h][b] = 2; //2 = Busch
					}
					else if (zufallsZahl >= 145 && spielFeld[h][b] == 0){
						spielFeld[h][b] = 5; //5 = Stein
					}
					else if(spielFeld[h][b] == 0){
						spielFeld[h][b] = 10; //10 = Waldboden
					}
				}
			}
			
		}
		spielFeld = waterCleanup(spielFeld, hoehe, breite);
		return spielFeld;
	}	
	
	private static int[][] waterCleanup(int[][] field, int gesamthoehe, int gesamtlaenge){
		//wasserfeld cleanup, einzelne felder
		for (int hoehe = 0; hoehe < gesamthoehe; hoehe++) {
			for (int laenge = 0; laenge < gesamtlaenge; laenge++) {
				//Wenn H�he nicht 0 und L�nge nicht 0 und H�he nicht letzte und L�nge nicht letzte
				if (hoehe != 0 && laenge != 0 && hoehe != (gesamthoehe - 1) && laenge != (gesamtlaenge - 1)) {
					
					//Wenn Feld[oben], Feld[links], Feld[rechts], Feld[unten] = KEIN WASSER und Feld[x] = WASSER
					if (field[hoehe - 1][laenge] != 4 && field[hoehe][laenge - 1] != 4
							&& field[hoehe][laenge + 1] != 4 && field[hoehe + 1][laenge] != 4
							&& field[hoehe][laenge] == 4) {
						
						//Wenn 4 nicht von wasser umrundet, dann Boden
						field[hoehe][laenge] = 10;
					
					}
					
				//Wenn links am rand (nicht oben/unten/rechts)
				} else if (hoehe != 0 && laenge == 0 && hoehe != (gesamthoehe - 1) && laenge != (gesamtlaenge - 1)) {
					
					//Wenn Feld[oben], Feld[rechts], Feld[unten] = KEIN WASSER und Feld[x] = WASSER
					if (field[hoehe - 1][laenge] != 4 && field[hoehe + 1][laenge] != 4
							&& field[hoehe][laenge + 1] != 4 && field[hoehe][laenge] == 4) {
						
						//rand links, wenn nicht weiter umrundet dann Boden
						field[hoehe][laenge] = 10;
					
					}
					
				//Wenn rechts am rand (nicht oben/unten/links)
				} else if (hoehe != 0 && laenge != 0 && hoehe != (gesamthoehe - 1) && laenge == (gesamtlaenge - 1)) {
					
					//Wenn Feld[oben], Feld[links], Feld[unten] = KEIN WASSER und Feld[x] = WASSER
					if (field[hoehe - 1][laenge] != 4 && field[hoehe + 1][laenge] != 4
							&& field[hoehe][laenge - 1] != 4 && field[hoehe][laenge] == 4) {
						
						//rand rechts, wenn nicht weiter umrundet, dann Boden
						field[hoehe][laenge] = 10;
					}
				}

				//Wenn nirgends am rand
				if (hoehe != 0 && laenge != 0 && hoehe != (gesamthoehe - 1) && laenge != (gesamtlaenge - 1)) {
					
					//wenn leeres feld von wasser umrundet
					if (field[hoehe - 1][laenge] == 4 && field[hoehe][laenge - 1] == 4
							&& field[hoehe][laenge + 1] == 4 && field[hoehe + 1][laenge] == 4
							&& field[hoehe][laenge] != 4) {
						
						//wenn 0 von wasser umrundet, dann wasser
						field[hoehe][laenge] = 4;
					}
				}
			}
		}
		return field;
	}
}

