package net.smurtgame.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

import net.smurtgame.Launcher;

public class GUIMessage extends GUIElement {

	private String message;

	private Font drawOptions;

	public GUIMessage(Rectangle rect, int zPos, String message) {
		super(rect, zPos);
		this.message = message;

		drawOptions = new Font("Arial", Font.PLAIN, 80);

	}

	@Override
	public void render(Graphics2D g) {
		
		super.render(g);
		drawCenteredString(message, Launcher.DRAWABLE_WIDTH,Launcher.DRAWABLE_HEIGHT,g);
		
	}

	public void drawCenteredString(String s, int w, int h, Graphics2D g) {
		g.setColor(Color.white);
		g.setFont(this.drawOptions);
		FontMetrics fm = g.getFontMetrics();
		int x = (w - fm.stringWidth(s)) / 2;
		int y = (fm.getAscent() + (h - (fm.getAscent() + fm.getDescent())) / 2);
		g.drawString(s, x, y);
	}

}
