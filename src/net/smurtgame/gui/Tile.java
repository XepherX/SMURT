package net.smurtgame.gui;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;

import net.smurtgame.ImageLoader;
import net.smurtgame.Launcher;

public class Tile extends ImageElement{
	

	private int walkOptions; 
	private int tileID;
	
	public int getTileID()
	{
		return this.tileID;
		
	}
	
	public void setTileID(int tileID)
	{
		this.tileID = tileID;
	}
	
	public int getWalkOptions() {
		return walkOptions;
	}

	public void setWalkOptions(int walkOptions) {
		this.walkOptions = walkOptions;
	}

	public boolean highlighted = false;
	public Tile(Image img, Rectangle rect, int zIndex) {
		super(rect, zIndex, img, Launcher.SCALE);
	}

	public Tile(Image img, int zIndex, int walkOptions, int tileId)
	{
		super(new Rectangle(),zIndex,img,Launcher.SCALE);
		this.tileID = tileId;
		this.walkOptions = walkOptions;

		super.position.width = img.getWidth(null) * Launcher.SCALE;
		super.position.height = img.getHeight(null) * Launcher.SCALE ;
		
		
		
		
	}
	
	public void render(Graphics2D g)
	{
		super.render(g);
		g.setColor(Color.red);
		if(highlighted)
			g.fillRect(super.position.x + super.xOffset , super.position.y + super.yOffset , super.position.width, super.position.height);
			
	}
	
	public static Tile fromID(int tileID)
	{
		Tile ret = null;
		switch(tileID){
			case 0: // Waldboden
				ret = new Tile(ImageLoader.loadImage("/artwork/tiles/Waldboden1.png"), 1, 0, 0);
				break;
			case 1: // Baumspitze
				ret = new Tile(ImageLoader.loadImage("/artwork/tiles/BaumA.png"), 1, 1, 1);
				break;
			case 2: // Busch
				ret = new Tile(ImageLoader.loadImage("/artwork/tiles/WaldbodenStrauch.png"), 1, 2, 2);
				break;
			case 3: // Wasser
				ret = new Tile(ImageLoader.loadImage("/artwork/tiles/Wasser2.png"), 1, 3, 3);
				break;
			case 4: // Stein
				ret = new Tile(ImageLoader.loadImage("/artwork/tiles/WaldbodenStein.png"), 1, 2, 4);
				break;
			case 5: // basis
				ret = new Tile(ImageLoader.loadImage("/artwork/tiles/Base.png"), 1, 0, 5);
				break;
			case 6: // baumstumpf
				ret = new Tile(ImageLoader.loadImage("/artwork/tiles/BaumB.png"), 1, 2, 6);
				break;
				
				
				
			
		}
		
			
		return ret;
	}
	
	public String toString()
	{
		return this.tileID+"";
	}
	

	


}
