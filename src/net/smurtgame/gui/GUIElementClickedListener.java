package net.smurtgame.gui;

public interface GUIElementClickedListener {
	public void onClicked(GUIElement guiElement);
	public void onRightClicked(GUIElement guiElement);
}
