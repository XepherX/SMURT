package net.smurtgame.gui;

import java.util.ArrayList;

import net.smurtgame.network.RemotePlayer;

public interface PlayerFoundListener {
	public void onPlayerFound(RemotePlayer player);
	public void onPlayerlistReceived(ArrayList<RemotePlayer> players);
	public void onRemoteplayerStarted(RemotePlayer player);
}
