package net.smurtgame.gui;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collections;

import net.smurtgame.Panzer;

//Class Gui
public class GUIPanel {

	public ArrayList<GUIElement> elements = new ArrayList<GUIElement>();

	public int mouseX;
	public int mouseY;

	public void tick() {
		for (GUIElement element : elements)
			element.tick();
	}

	public void render(Graphics2D g) {

		if (elements == null)
			return;
		synchronized (this.elements) {
			for (GUIElement element : elements) {
				if(element instanceof Panzer){
					Panzer p = (Panzer) element;
					if (p.getHealth()>0)
						element.render(g);
				}else
					element.render(g);
			}
		}

	}

	public synchronized void clickedAt(int x, int y) {
		for (GUIElement element : elements) {
			element.isClicked(x, y);
		}
	}

	public void addElement(GUIElement e) {
		addElementUnsorted(e);
		zSort();

	}

	public void addElementUnsorted(GUIElement e) {
		elements.add(e);

	}

	public void zSort() {
		Collections.sort(elements);
	}

	public void dragged(int x, int y) {

	}

	public void rightClickedAt(int x, int y) {

		for (GUIElement element : elements) {

			try {
				/*
				 * Panzer tmp = (Panzer) element; tmp.deselect();
				 * tmp.setRenderMoveto(false);
				 */
				element.rightClicked();

			} catch (Exception ex) {
				continue;
			}
			;

		}

	}

}
