package net.smurtgame.gui;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.io.PrintWriter;
import java.net.DatagramSocket;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import net.smurtgame.Game;
import net.smurtgame.ImageLoader;
import net.smurtgame.Launcher;
import net.smurtgame.network.NetUtils;
import net.smurtgame.network.NetworkClient;
import net.smurtgame.network.NetworkServer;
import net.smurtgame.network.PacketReceivedListener;
import net.smurtgame.network.PlayerPanel;
import net.smurtgame.network.RemotePlayer;
import net.smurtgame.network.packet.DiscoveryPacket;
import net.smurtgame.network.packet.GameStartPacket;
import net.smurtgame.network.packet.Packet;

public class SearchPlayerPanel extends GUIPanel implements PlayerFoundListener,PacketReceivedListener, GUIElementClickedListener{
	
	public class PlayerSeeker extends Thread{
		private SearchPlayerPanel searchPlayerPanel;
		
		private SearchPlayerPanel parent;
		public boolean isServer = false;
		NetworkClient nc  = new NetworkClient("255.255.255.255");
		NetworkServer ns;
		
		public PlayerSeeker(SearchPlayerPanel pan)
		{
			this.parent = pan;
		}
		public void run(){
			DatagramSocket socket = null;
			PrintWriter out = null;
			
			
			if(JOptionPane.showConfirmDialog(null, "Bist du der Server?") == 0){
				ns = new NetworkServer();
				ns.start();
				isServer = true;
			}
			
			
			if(isServer)
				ns.addOnPlayerFoundListener(parent);
			else
				nc.addOnPlayerFoundListener(parent);
			
			
			nc.addOnPacketReceivedListener(parent);
			
			nc.start();
			//nc.sendData("ping".getBytes());
			String name = JOptionPane.showInputDialog("Soldat, gib mir deinen Namen bekannt!");
			if(name != null)
			{
				this.parent.game.setName(name);
				System.out.println("set the name to "+this.parent.game.getYou().getName());
				nc.sendPacket(new DiscoveryPacket(name,nc.ipAddress));
			}

		}
		
		public void addPlayerFoundListener(PlayerFoundListener p){
			
		}
		
		public void connectToPlayer(RemotePlayer player)
		{
			//this.parent.game.set
			System.out.println("making player with name "+this.parent.game.getYou().getName());
			GameStartPacket start = new GameStartPacket(this.parent.game.getYou().getName(), nc.ipAddress);
			
			if(this.isServer)
			{
				ns.sendPacket(start, player);
			} else {
				nc.sendPacket(start);
			}
			
			this.parent.game.connectToPlayer(player,this);
		}
		
		public ArrayList<RemotePlayer> connectedPlayers()
		{
			if(isServer){
				return ns.connectedPlayers();
			} else {
				return nc.connectedPlayers();
			}
				
			
		}
		
	}
	
	
	private PlayerSeeker playerSeeker;
	private Game game;
	private Image playerBackdrop = ImageLoader.loadImage("/artwork/PlayerListBackground.png");
	

	public SearchPlayerPanel(Game game){
		elements.add(new ImageElement(0,ImageLoader.loadImage("/artwork/PlayerListBackground.png"),1));
		this.game = game;
		playerSeeker = new PlayerSeeker(this);		
		playerSeeker.addPlayerFoundListener(this);
		playerSeeker.start();
		
	}

	@Override
	public void onPlayerFound(RemotePlayer player) {

			System.out.println("Player "+player.getPlayerName()+" found");
			
			if(!NetUtils.isMe(player.getSocketAddress().getAddress())){
				Rectangle pos = new Rectangle();
				pos.x = 0;
				pos.y = (elements.size()-1) * 100 - 10;
				pos.height = 100;
				pos.width = Launcher.DRAWABLE_WIDTH;
				
				PlayerPanel panel = new PlayerPanel(pos, 1,ImageLoader.loadImage("/artwork/PlayerList.png"),1);
				panel.setAction("connect");
				panel.setPlayer(player);
				panel.addClickedListener(this);
				this.addElement(panel);	
			} else {
				System.out.println("not adding, this is me!");
			}
			
			
			
		
	}

	@Override
	public void onPacketReceived(Packet packet) {
		System.out.println("Received Packet from "+packet.getSender().toString());
		
		
		
		
		
	}

	@Override
	public void render(Graphics2D g) {
		super.render(g);
		
	
	}

	@Override
	public void onClicked(GUIElement guiElement) {
		
		if(guiElement.getAction() == "connect")
		{
			
			PlayerPanel selected = (PlayerPanel)guiElement;
			
			System.out.println("connecting to player "+selected.getPlayer().getPlayerName()+"("+selected.getPlayer().getSocketAddress().getHostName()+")");
			this.playerSeeker.connectToPlayer(selected.getPlayer());
			
		}
		
	}

	@Override
	public void onRightClicked(GUIElement guiElement) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPlayerlistReceived(ArrayList<RemotePlayer> players) {
		
		synchronized(this.elements){
			elements.clear();
			elements.add(new ImageElement(0,ImageLoader.loadImage("/artwork/PlayerListBackground.png"),1));
			
			for(RemotePlayer player : players){
				this.onPlayerFound(player);
			}
			
		}
		
	}

	@Override
	public void onRemoteplayerStarted(RemotePlayer player) {
		System.out.println("remote player "+player.getPlayerName()+" initiated the game...");
		
		this.game.connectToPlayer(player, this.playerSeeker);
		
		
	}
	

}
