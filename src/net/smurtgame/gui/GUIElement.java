package net.smurtgame.gui;


import java.awt.Graphics2D;
import java.awt.List;
import java.awt.Rectangle;
import java.util.ArrayList;


public class GUIElement implements Comparable{

	protected Rectangle position;
	private int zIndex;
	private String action = "";
	protected ArrayList<GUIElementClickedListener> listeners = new ArrayList<GUIElementClickedListener>();
	
	public String getAction(){
		return this.action;
	}
	public void setAction(String s){
		this.action = s;
	}
	public void tick(){
		
	}
	
	public void render(Graphics2D g){
		
	}
	
	public Rectangle getPos(){
		return this.position;
	}
	
	public void setPos(Rectangle pos){
		this.position = pos;
	}
	
	public GUIElement(Rectangle rect, int zPos)
	{
		this.position = rect;
		this.zIndex = zPos;
	}
	
	public GUIElement(Rectangle rect, int zPos, String action)
	{
		this.position = rect;
		this.zIndex = zPos;
		this.action = action;
	}
	
	public boolean isClicked(int clickX, int clickY){
		
		if(!position.contains(clickX, clickY))
			return false;
		for(GUIElementClickedListener listener : listeners){
			listener.onClicked(this);
		}
		
		return true;
		
	}
	
	public void addClickedListener(GUIElementClickedListener listener){
		listeners.add(listener);
	}
	
	

	@Override
	public int compareTo(Object o) {
		GUIElement compare = (GUIElement) o;

		return (zIndex - compare.zIndex );
		
	}
	public void rightClicked() {

		for(GUIElementClickedListener listener : listeners){
			listener.onRightClicked(this);
		}
	}


	

}
