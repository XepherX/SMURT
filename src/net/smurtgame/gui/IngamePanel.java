package net.smurtgame.gui;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import net.smurtgame.Game;
import net.smurtgame.Panzer;
import net.smurtgame.Spielfeld;
import net.smurtgame.gui.SearchPlayerPanel.PlayerSeeker;
import net.smurtgame.network.NetUtils;
import net.smurtgame.network.PacketReceivedListener;
import net.smurtgame.network.packet.GameHealthUpdatePacket;
import net.smurtgame.network.packet.GameTilearray;
import net.smurtgame.network.packet.GameTurnsyncPacket;
import net.smurtgame.network.packet.Packet;

public class IngamePanel extends GUIPanel implements GUIElementClickedListener, PacketReceivedListener {
	private ArrayList<Panzer> panzerPLayer1 = new ArrayList<Panzer>();
	private ArrayList<Panzer> panzerPLayer2 = new ArrayList<Panzer>();
	private int fieldWidth;
	private int fieldHeight;
	private boolean moveTank;
	private Panzer tankToMove;
	private boolean serverTurn = true;
	private String winPlayer = null;

	private Spielfeld spielfeld;

	private PlayerSeeker connection;

	private Game game;

	public ArrayList<Panzer> getPanzerPLayer1() {
		return panzerPLayer1;
	}

	public void setPanzerPLayer1(ArrayList<Panzer> panzerPLayer1) {
		this.panzerPLayer1 = panzerPLayer1;
	}

	public ArrayList<Panzer> getPanzerPLayer2() {
		return panzerPLayer2;
	}

	public void setPanzerPLayer2(ArrayList<Panzer> panzerPLayer2) {
		this.panzerPLayer2 = panzerPLayer2;
	}

	public IngamePanel(Spielfeld feld, Game game, PlayerSeeker connection) {
		this.spielfeld = feld;
		this.game = game;
		this.connection = connection;
		
		if(connection != null && connection.isServer)
		{
			GameTilearray packet = new GameTilearray(feld.getTilearr(),NetUtils.getMe());
			connection.ns.sendPacket(packet, game.getEnemy());
			
		}
		
		
	}

	public void render(Graphics2D g) {
		super.render(g);
		checkTurn(g);
		if (moveTank) {
			g.setColor(Color.red);
			int rectX = ((mouseX - spielfeld.position.x) / 32) * 32 + spielfeld.position.x - 1;
			int rectY = ((mouseY - spielfeld.position.y) / 32) * 32 + spielfeld.position.y - 1;

			if (rectX / 32 >= 29)
				rectX = 29 * 32 + spielfeld.position.x;
			if (rectY / 32 >= 18)
				rectY = 18 * 32 + spielfeld.position.y;

			g.drawRect(rectX, rectY, 32, 32);
		}
	}

	@Override
	public void onClicked(GUIElement guiElement) {
		if (guiElement.getAction() == "moveTank" && !moveTank && tankToMove == null) {
			tankToMove = (Panzer) guiElement;
			if(connection!= null && (connection.isServer && tankToMove.getId() < 6 || !connection.isServer && tankToMove.getId() > 5 )|| connection == null)
			

			// Kontroliert ob panzer am Zug ist, movePoints �brig hat und noch
			// am leben ist
			if (serverTurn && tankToMove.getId() < 6 && tankToMove.getMovePoints() > 0 && tankToMove.getHealth() > 0 || !serverTurn && tankToMove.getId() > 5 && tankToMove.getMovePoints() > 0 && tankToMove.getHealth() > 0) {
				moveTank = true;
				setMoveOptions(tankToMove);
				setAttackOptions(tankToMove);
				tankToMove.move();
			} else {
				tankToMove = null;
			}
		}

		if (guiElement.getAction() == "endTurn") {
			System.out.println("end turn clicked");
			
			this.endTurn();
		}
		if(guiElement.getAction() == "goBack")
		{
			if(JOptionPane.showConfirmDialog(null, "You will forfeit the current game. Are you sure?","Warning!",JOptionPane.OK_CANCEL_OPTION) == 0)				
				this.game.setMenu(GUIPanelFactory.TitleScreen(this.game));
		}
		if (guiElement.getAction() == "tileclick" && moveTank && tankToMove != null) {

			Tile t = (Tile) guiElement;

			/*
			 * tankToMove.position.x = t.position.x + t.getxOffset();
			 * tankToMove.position.y = t.position.y + t.getyOffset();
			 */
			if (tankToMove.moveCancelled()) {
				System.out.println("move cancelled.");
				moveTank = false;
				System.out.println("moveTank = false");
				tankToMove = null;
				return;

			}
			Rectangle newPos = t.position;

			// Kontroliert ob das gew�nste Tile in Reichweite des Panzers ist

			for (Rectangle r : tankToMove.getPosibleDirections()) {

				if (newPos.contains(new Point(r.x, r.y))) {
					// if (newPos.intersects(r)) {
					tankToMove.movedTo(t.position);

					// checkTurn();
					if (connection != null) { // delete when network mode is
												// done
						if (connection.isServer)
							connection.ns.sendPacket(tankToMove.toPacket(), game.getEnemy());
						else
							connection.nc.sendPacket(tankToMove.toPacket());
					}
					tankToMove = null;
					moveTank = false;
					return;
				}
			}

			ArrayList<Panzer> tmpPanzer;

			// kontroliert ob auf dem gew�nsten tile ein angreifbarer panzer ist

			for (Rectangle r : tankToMove.getPosibleTargets())
				if (newPos.contains(new Point(r.x, r.y))) {
					if (serverTurn)
						tmpPanzer = panzerPLayer2;
					else
						tmpPanzer = panzerPLayer1;
					for (Panzer p : tmpPanzer)

						if (p.position.contains(new Point(r.x, r.y))) {
							tankToMove.angreifen(p);
							if (p.getHealth() < 0) {
								tmpPanzer.remove(p);
							}
							// checkTurn();
							moveTank = false;
							if (tankToMove != null) {
								tankToMove.deselect();
								tankToMove = null;
							}
							
							System.out.println("tanktomove: "+tankToMove);
							if(connection != null)
							{
								if(this.connection.isServer)
								{
									connection.ns.sendPacket(new GameHealthUpdatePacket(p,NetUtils.getMe()), game.getEnemy());
								} else {
									connection.nc.sendPacket(new GameHealthUpdatePacket(p,NetUtils.getMe()));
								}
								// besiegte Panzer werden von der Liste entfernt
								
							}
							break;
						}

				}

		}

	}

	private void setAttackOptions(Panzer tankToMove) {

		boolean shot_down, shot_up, shot_left, shot_right;
		shot_down = shot_up = shot_left = shot_right = true;
		tankToMove.getPosibleTargets().clear();
		tankToMove.getMaximumRange().clear();
		int tmpCheckTank;
		int x = (tankToMove.position.x + this.spielfeld.position.x) / 32;
		int y = (tankToMove.position.y + this.spielfeld.position.y) / 32;
		for (int i = 1; i <= tankToMove.getRange(); i++) {
			if (tankToMove.position.x + tankToMove.position.width * i <= 928 && shot_right) {
				tmpCheckTank = CheckForTanks(new Rectangle(tankToMove.position.x + tankToMove.position.width * i, tankToMove.position.y, 32, 32));
				if (this.spielfeld.getTilearr()[y][x + i].getWalkOptions() != 0 && this.spielfeld.getTilearr()[y][x + i].getWalkOptions() != 3 || tmpCheckTank == 1)
					shot_right = false || this.spielfeld.getTilearr()[y][x + i].getWalkOptions() == 4;
				if (shot_right)
					if (tmpCheckTank == 2) {

						tankToMove.getPosibleTargets().add(new Rectangle(tankToMove.position.x + this.spielfeld.position.x + tankToMove.position.width * i, tankToMove.position.y + this.spielfeld.position.y, 32, 32));
						shot_right = false;
					} else if (i > tankToMove.getMovement())
						tankToMove.getMaximumRange().add(new Rectangle(tankToMove.position.x + this.spielfeld.position.x + tankToMove.position.width * i, tankToMove.position.y + this.spielfeld.position.y, 32, 32));
			}
			if (tankToMove.position.x - tankToMove.position.width * i >= 0 && shot_left) {
				tmpCheckTank = CheckForTanks(new Rectangle(tankToMove.position.x - tankToMove.position.width * i, tankToMove.position.y, 32, 32));
				if (this.spielfeld.getTilearr()[y][x - i].getWalkOptions() != 0 && this.spielfeld.getTilearr()[y][x - i].getWalkOptions() != 3 || tmpCheckTank == 1)
					shot_left = false || this.spielfeld.getTilearr()[y][x - i].getWalkOptions() == 4;
				if (shot_left)
					if (tmpCheckTank == 2) {
						tankToMove.getPosibleTargets().add(new Rectangle(tankToMove.position.x + this.spielfeld.position.x - tankToMove.position.width * i, tankToMove.position.y + this.spielfeld.position.y, 32, 32));
						shot_left = false;
					} else if (i > tankToMove.getMovement())
						tankToMove.getMaximumRange().add(new Rectangle(tankToMove.position.x + this.spielfeld.position.x - tankToMove.position.width * i, tankToMove.position.y + this.spielfeld.position.y, 32, 32));
			}
			if (tankToMove.position.y - tankToMove.position.width * i >= 0 && shot_up) {
				tmpCheckTank = CheckForTanks(new Rectangle(tankToMove.position.x, tankToMove.position.y - tankToMove.position.width * i, 32, 32));
				if (this.spielfeld.getTilearr()[y - i][x].getWalkOptions() != 0 && this.spielfeld.getTilearr()[y - i][x].getWalkOptions() != 3 || tmpCheckTank == 1)
					shot_up = false || this.spielfeld.getTilearr()[y - i][x].getWalkOptions() == 4;
				if (shot_up)
					if (tmpCheckTank == 2) {
						tankToMove.getPosibleTargets().add(new Rectangle(tankToMove.position.x + this.spielfeld.position.x, tankToMove.position.y - tankToMove.position.width * i + this.spielfeld.position.y, 32, 32));
						shot_up = false;
					} else if (i > tankToMove.getMovement())
						tankToMove.getMaximumRange().add(new Rectangle(tankToMove.position.x + this.spielfeld.position.x, tankToMove.position.y - tankToMove.position.width * i + this.spielfeld.position.y, 32, 32));
			}
			if (tankToMove.position.y + tankToMove.position.width * i <= 576 && shot_down) {
				tmpCheckTank = CheckForTanks(new Rectangle(tankToMove.position.x, tankToMove.position.y + tankToMove.position.width * i, 32, 32));
				if (this.spielfeld.getTilearr()[y + i][x].getWalkOptions() != 0 && this.spielfeld.getTilearr()[y + i][x].getWalkOptions() != 3 || tmpCheckTank == 1)
					shot_down = false || this.spielfeld.getTilearr()[y + i][x].getWalkOptions() == 4;
				if (shot_down)
					if (tmpCheckTank == 2) {
						tankToMove.getPosibleTargets().add(new Rectangle(tankToMove.position.x + this.spielfeld.position.x, tankToMove.position.y + this.spielfeld.position.y + tankToMove.position.height * i, 32, 32));
						shot_down = false;
					} else if (i > tankToMove.getMovement())
						tankToMove.getMaximumRange().add(new Rectangle(tankToMove.position.x + this.spielfeld.position.x, tankToMove.position.y + tankToMove.position.width * i + this.spielfeld.position.y, 32, 32));
			}
		}
	}

	// legt die M�glichen Bewegungs und angriffsoptionen f�r einen Panzer fest
	public void setMoveOptions(Panzer tankToMove) {
		boolean move_up, move_down, move_left, move_right;
		move_up = move_down = move_left = move_right = true;
		tankToMove.getPosibleDirections().clear();
		int tmpCheckTank;
		int x = (tankToMove.position.x + this.spielfeld.position.x) / 32;
		int y = (tankToMove.position.y + this.spielfeld.position.y) / 32;
		for (int i = 1; i <= tankToMove.getMovement(); i++) {
			if (tankToMove.position.x + tankToMove.position.width * i <= 928 && move_right) {
				tmpCheckTank = CheckForTanks(new Rectangle(tankToMove.position.x + tankToMove.position.width * i, tankToMove.position.y, 32, 32));
				if (this.spielfeld.getTilearr()[y][x + i].getWalkOptions() != 0 || tmpCheckTank > 0) {
					move_right = false;
				}
				if (move_right)
					tankToMove.getPosibleDirections().add(new Rectangle(tankToMove.position.x + this.spielfeld.position.x + tankToMove.position.width * i, tankToMove.position.y + this.spielfeld.position.y, 32, 32));
			}
			if (tankToMove.position.x - tankToMove.position.width * i >= 0 && move_left) {
				tmpCheckTank = CheckForTanks(new Rectangle(tankToMove.position.x - tankToMove.position.width * i, tankToMove.position.y, 32, 32));
				if (this.spielfeld.getTilearr()[y][x - i].getWalkOptions() != 0 || tmpCheckTank > 0)
					move_left = false;
				if (move_left)
					tankToMove.getPosibleDirections().add(new Rectangle(tankToMove.position.x + this.spielfeld.position.x - tankToMove.position.width * i, tankToMove.position.y + this.spielfeld.position.y, 32, 32));
			}
			if (tankToMove.position.y - tankToMove.position.width * i >= 0 && move_up) {
				tmpCheckTank = CheckForTanks(new Rectangle(tankToMove.position.x, tankToMove.position.y - tankToMove.position.width * i, 32, 32));
				if (this.spielfeld.getTilearr()[y - i][x].getWalkOptions() != 0 || tmpCheckTank > 0)
					move_up = false;
				if (move_up)
					tankToMove.getPosibleDirections().add(new Rectangle(tankToMove.position.x + this.spielfeld.position.x, tankToMove.position.y + this.spielfeld.position.y - tankToMove.position.height * i, 32, 32));
			}
			if (tankToMove.position.y + tankToMove.position.width * i <= 576 && move_down) {
				tmpCheckTank = CheckForTanks(new Rectangle(tankToMove.position.x, tankToMove.position.y + tankToMove.position.width * i, 32, 32));
				if (this.spielfeld.getTilearr()[y + i][x].getWalkOptions() != 0 || tmpCheckTank > 0)
					move_down = false;
				if (move_down)
					tankToMove.getPosibleDirections().add(new Rectangle(tankToMove.position.x + this.spielfeld.position.x, tankToMove.position.y + this.spielfeld.position.y + tankToMove.position.height * i, 32, 32));
			}
		}

	}

	private void checkTurn(Graphics2D g) {
		g.setColor(new Color(0, 255, 0, 30));
		boolean newTurn;
		newTurn = true;

		if (serverTurn) {
			for (Panzer p : panzerPLayer1) {
				if (p.getMovePoints() > 0 && p.getHealth() >= 0) {
					g.fillRect(p.position.x + this.spielfeld.position.x, p.position.y + this.spielfeld.position.y, 32, 32);
					newTurn = false;
				}
			}
		} else {
			for (Panzer p : panzerPLayer2) {
				if (p.getMovePoints() > 0 && p.getHealth() >= 0) {
					g.fillRect(p.position.x + this.spielfeld.position.x, p.position.y + this.spielfeld.position.y, 32, 32);
					newTurn = false;
				}
			}
		}
		if (panzerPLayer1.size() < 1)
			finish((this.game.getEnemy().getName()), g);
		if (panzerPLayer2.size() < 1)
			finish((this.game.getYou().getName()), g);

		if (newTurn) {
			endTurn();
		}
	}

	private void endTurn() {
		System.out.println("endturn method");
		System.out.println("tanktomove: "+tankToMove);
		if (tankToMove == null) {
			serverTurn = !serverTurn;
			System.out.println("toggled serverturn to "+serverTurn);
			if (serverTurn)
				for (Panzer p : panzerPLayer1)
					p.resetMovePoints();
			else
				for (Panzer p : panzerPLayer2)
					p.resetMovePoints();
			
			System.out.println("con is "+connection);
			if(connection != null)
			{
				System.out.println("sending turnsync");
				if(connection.isServer)
				{
					connection.ns.sendPacket(new GameTurnsyncPacket(serverTurn, NetUtils.getMe()), game.getEnemy());
					
				} else {
					connection.nc.sendPacket(new GameTurnsyncPacket(serverTurn, NetUtils.getMe()));
				}
			}
		}
		
	}

	// �berpr�ft ob auf bestimmten Tiles ein Panzer ist
	// 0 = kein panzer
	// 1 = verb�ndeter Panzer
	// 2 = gegnerischer Panzer
	public int CheckForTanks(Rectangle r) {
		if (serverTurn) {
			for (Panzer p : panzerPLayer1)
				if (p.position.intersects(r) && p.getHealth() > 0)
					return 1;
			for (Panzer p : panzerPLayer2)
				if (p.position.intersects(r) && p.getHealth() > 0)
					return 2;
			return 0;
		} else {
			for (Panzer p : panzerPLayer2)
				if (p.position.intersects(r) && p.getHealth() > 0)
					return 1;
			for (Panzer p : panzerPLayer1)
				if (p.position.intersects(r) && p.getHealth() > 0)
					return 2;
			return 0;
		}
	}
	private boolean allredyDisplayed = false;;
	public void finish(String name, Graphics2D g) {
		
		if(!allredyDisplayed)
		{
			GUIMessage finished = new GUIMessage(new Rectangle(20,20,20,20),2,name+" hat gewonnen");
			this.elements.add(finished);
			allredyDisplayed  = true;
		}
		
		

		// game.setMenu(GUIPanelFactory.TitleScreen(game));
	}

	@Override
	public void onRightClicked(GUIElement guiElement) {

		moveTank = false;
		if (tankToMove != null) {
			tankToMove.deselect();
			tankToMove = null;
		}
	}

	@Override
	public void onPacketReceived(Packet packet) {
		System.out.println("IGP received packet "+packet.getPacketId());
		if(packet.getPacketId() == Packet.PACKET_GAME_TILES)
		{
			System.out.println("received tiles from server.");
			GameTilearray tiles = GameTilearray.fromString(packet.getRawMessage(),packet.getSender());
			
			synchronized( super.elements)
			{
				
				Spielfeld newField = new Spielfeld(spielfeld.position,1,tiles.getTilearray());
				super.elements.remove(this.spielfeld);
				this.addElement(newField);
				this.spielfeld = newField;
				newField.addClickedListener(this);
				
				
			}
			
			
			
		}
		if(packet.getPacketId() == Packet.PACKET_GAME_HEALTH)
		{
			GameHealthUpdatePacket health = GameHealthUpdatePacket.fromString(packet.getRawMessage(), packet.getSender());
			Panzer toUpdate = null;
			ArrayList<Panzer> tmpPanzer = null;
			
			for(Panzer p : panzerPLayer1)
			{
				if(p.getId() == health.getTankId()){
					toUpdate = p;
					tmpPanzer = panzerPLayer1;
					System.out.println("tank was in pl1");
				}
					
			}
			
			for(Panzer p : panzerPLayer2)
			{
				if(p.getId() == health.getTankId())
				{
					toUpdate = p;
					tmpPanzer = panzerPLayer2;
					System.out.println("tank was in pl2");
				}
					
			}
			
			
			System.out.println("tank "+health.getTankId()+" has health "+health.getNewHealth());
			
			toUpdate.setHealth(health.getNewHealth());
			
			if (toUpdate.getHealth() < 0) {
				tmpPanzer.remove(toUpdate);
			}
		}
		if(packet.getPacketId() == Packet.PACKET_GAME_TURNSYNC)
		{
			GameTurnsyncPacket turnsync = GameTurnsyncPacket.fromString(packet.getRawMessage(), packet.getSender());
			System.out.println("received turnsync. should set to "+turnsync.getTurnSync()+", is "+serverTurn);
			tankToMove = null;
			if(turnsync.getTurnSync() != serverTurn)
			{
				this.endTurn();
			}
		}
		
	}

}
