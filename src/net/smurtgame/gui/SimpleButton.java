package net.smurtgame.gui;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;

public class SimpleButton extends GUIElement {
	public SimpleButton(Rectangle rekt, int zIndex)
	{
		super(rekt, zIndex); // # GET SUPER REKT
	
	}
	
	public synchronized void render(Graphics2D g)
	{
		int x = super.position.x;
		int y = super.position.y;
		
		int w = (int) super.position.getWidth();
		int h = (int) super.position.getHeight();
		
		g.setColor(Color.blue);
		g.fillRect(x, y, w, h);
	}
}
