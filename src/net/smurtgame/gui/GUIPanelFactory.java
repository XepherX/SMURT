package net.smurtgame.gui;

import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;

import net.smurtgame.Artillery;
import net.smurtgame.Game;
import net.smurtgame.Goliath;
import net.smurtgame.ImageLoader;
import net.smurtgame.Launcher;
import net.smurtgame.Leopard;
import net.smurtgame.Panzer;
import net.smurtgame.Spielfeld;
import net.smurtgame.SpielfeldFactory;
import net.smurtgame.gui.SearchPlayerPanel.PlayerSeeker;

public class GUIPanelFactory {
	public static GUIPanel TitleScreen(GUIElementClickedListener listener) {
		GUIPanel gp = new GUIPanel();
		Rectangle buttonPos = new Rectangle(Launcher.WIDTH * Launcher.SCALE / 2 - 325 / 2, Launcher.HEIGHT * Launcher.SCALE / 2 - 2 - 64 - 64 - 2, 325, 64);
		Rectangle buttonPos2 = new Rectangle(Launcher.WIDTH * Launcher.SCALE / 2 - 325 / 2, Launcher.HEIGHT * Launcher.SCALE / 2 - 64, 352, 64);
		Rectangle buttonPos3 =  new Rectangle(Launcher.WIDTH * Launcher.SCALE / 2 - 325 / 2, Launcher.HEIGHT * Launcher.SCALE / 2 + 4, 352, 64);
				
		SimpleButton b = new SimpleButton(buttonPos, 1);

		Image img = ImageLoader.loadImage("/artwork/title.png");

		Rectangle r = new Rectangle(0, 0, 0, 0);
		ImageElement i = new ImageElement(r, 0, img, 1);

		System.out.println(Launcher.DRAWABLE_WIDTH + " x " + Launcher.DRAWABLE_HEIGHT);

		Image startGame = ImageLoader.loadImage("/artwork/multiplayer.png");
		Image startGame2 = ImageLoader.loadImage("/artwork/singleplayer.png");
		Image howto = ImageLoader.loadImage("/artwork/lobbyTutorial.png");

		ImageElement start = new ImageElement(buttonPos2, 4, startGame, 1, "multiplayer");
		ImageElement local = new ImageElement(buttonPos, 4, ImageLoader.loadImage("/artwork/Localgame.png"), 1, "singleplayer");
		ImageElement tutorial = new ImageElement(buttonPos3, 4, howto, 1, "tutorial");
		
		start.addClickedListener(listener);
		local.addClickedListener(listener);
		tutorial.addClickedListener(listener);

		gp.addElement(i);
		gp.addElement(local);
		gp.addElement(start);
		gp.addElement(tutorial);

		return gp;
	}

	public static GUIPanel SearchPlayerScreen(Game game) {
		SearchPlayerPanel spp = new SearchPlayerPanel(game);

		return spp;
	}

	public static GUIPanel InGameScreen(Game game, PlayerSeeker connection) {
		int[][] intField = null;
		//SpielfeldFactory spFactory = new SpielfeldFactory();
		// for testing, remove when done
		if (connection == null) {
			int zufallsZahl = ((int) (Math.random() * (4 - 1) + 1));
			if (zufallsZahl == 1) {
				intField = SpielfeldFactory.createForest(15, 30);
			} else if (zufallsZahl == 2) {
				intField = SpielfeldFactory.createLake(15, 30);
			} else if (zufallsZahl == 3) {
				intField = SpielfeldFactory.createPlains(15, 30);
			}
		} else {

			if (connection.isServer) {

				int zufallsZahl = ((int) (Math.random() * (4 - 1) + 1));
				if (zufallsZahl == 1) {
					intField = SpielfeldFactory.createForest(15, 30);
				} else if (zufallsZahl == 2) {
					intField = SpielfeldFactory.createLake(15, 30);
				} else if (zufallsZahl == 3) {
					intField = SpielfeldFactory.createPlains(15, 30);
				}
			} else {
				int zufallsZahl = ((int) (Math.random() * (4 - 1) + 1));
				if (zufallsZahl == 1) {
					intField = SpielfeldFactory.createForest(15, 30);
				} else if (zufallsZahl == 2) {
					intField = SpielfeldFactory.createLake(15, 30);
				} else if (zufallsZahl == 3) {
					intField = SpielfeldFactory.createPlains(15, 30);
				}
			}
		}

		Tile[][] tiles = new Tile[19][30];

		// Alle verschiedenen Tiles hinzugefügt
		for (int i = 0; i < 19; i++) {
			for (int j = 0; j < 30; j++) {
				// Bodenteile
				if (intField[i][j] == 10) {// 10 Waldboden
					tiles[i][j] = new Tile(ImageLoader.loadImage("/artwork/tiles/Waldboden1.png"), 1, 0, 0);

				} 
				// Elemente
				else if (intField[i][j] == 1) {// Baumspitze TileID 1
					tiles[i][j] = new Tile(ImageLoader.loadImage("/artwork/tiles/BaumA.png"), 1, 1, 1);

				} 
				else if(intField[i][j]==2){// Busch TileID 2
					tiles[i][j] = new Tile(ImageLoader.loadImage("/artwork/tiles/WaldbodenStrauch.png"), 1, 2, 2);
				} else if (intField[i][j] == 4) {// Wasser TileID 3
					tiles[i][j] = new Tile(ImageLoader.loadImage("/artwork/tiles/Wasser2.png"), 1, 3, 3);
				} else if (intField[i][j] == 5) {// Stein TileID 4
					tiles[i][j] = new Tile(ImageLoader.loadImage("/artwork/tiles/WaldbodenStein.png"), 1, 2, 4);
				} else if (intField[i][j] == 8) {// Basis TileID 5
					tiles[i][j] = new Tile(ImageLoader.loadImage("/artwork/tiles/Base.png"), 1, 0, 5);
				} else if (intField[i][j] == 9) {// Baumstumpf TileID 6
					tiles[i][j] = new Tile(ImageLoader.loadImage("/artwork/tiles/BaumB.png"), 1, 2, 6);
				}

			}
		}

		Spielfeld s = new Spielfeld(new Rectangle(new Point()), 1, tiles);

		IngamePanel inGamePanel = new IngamePanel(s, game, connection);
		
		ImageElement endTurn = new ImageElement(new Rectangle(Launcher.DRAWABLE_WIDTH-95,Launcher.DRAWABLE_HEIGHT - 25,95,25),5,ImageLoader.loadImage("/artwork/Endturn.png"),1);
		endTurn.setAction("endTurn");
		endTurn.addClickedListener(inGamePanel);
		inGamePanel.addElement(endTurn);
		
		ImageElement goBack = new ImageElement(new Rectangle(0,Launcher.DRAWABLE_HEIGHT - 25,95,25),5,ImageLoader.loadImage("/artwork/GoBack.png"),1);
		goBack.setAction("goBack");
		goBack.addClickedListener(inGamePanel);
		inGamePanel.addElement(goBack);
		if(connection != null)
		{
			if(connection.isServer)
			{
				connection.ns.addOnPacketReceivedListener(inGamePanel);
			} 
			else 
			{
				connection.nc.addOnPacketReceivedListener(inGamePanel);	
			}
			
			
		}
			
		
		
		
		
		// tanks for server
		for (int i = 0; i < 2; i++) {
			Panzer g = new Leopard(ImageLoader.loadImage("/artwork/tanks/LeopardD.png"), new Rectangle(new Point((s.getWidthInTiles() - 13) * 32 - i * 5 * 32, 0)), 2);
			Panzer a = new Artillery(ImageLoader.loadImage("/artwork/tanks/ArtilleryD.png"), new Rectangle(new Point((s.getWidthInTiles() - 15) * 32 - i * 32, 0)), 2);
			Panzer l = new Goliath(ImageLoader.loadImage("/artwork/tanks/GoliathD.png"), new Rectangle(new Point((s.getWidthInTiles() - 11) * 32 - i * 9 * 32, 0)), 2);
			l.setId(i);
			a.setId(i + 2);
			g.setId(i + 4);
			g.setxOffset(s.position.x);
			g.setyOffset(s.position.y);
			a.setxOffset(s.position.x);
			a.setyOffset(s.position.y);
			l.setxOffset(s.position.x);
			l.setyOffset(s.position.y);
			g.setAction("moveTank");
			g.addClickedListener(inGamePanel);
			a.setAction("moveTank");
			a.addClickedListener(inGamePanel);
			l.setAction("moveTank");

			l.addClickedListener(inGamePanel);
			inGamePanel.getPanzerPLayer1().add(l);
			inGamePanel.getPanzerPLayer1().add(a);
			inGamePanel.getPanzerPLayer1().add(g);
			inGamePanel.addElement(g);
			inGamePanel.addElement(a);
			inGamePanel.addElement(l);
			if (connection != null) {// remove when network is done
				if (connection.isServer) {
					connection.ns.addRemoteableListener(l);
					connection.ns.addRemoteableListener(a);
					connection.ns.addRemoteableListener(g);
				} else {
					connection.nc.addRemoteableListener(l);
					connection.nc.addRemoteableListener(a);
					connection.nc.addRemoteableListener(g);
				}
			}

		}
		// tanks for client
		for (int i = 0; i < 2; i++) {
			Panzer l = new Leopard(ImageLoader.loadImage("/artwork/tanks/LeopardU.png"), new Rectangle(new Point((s.getWidthInTiles() - 13) * 32 - i * 5 * 32, (s.getHeightInTiles() - 1) * 32)), 2);
			Panzer a = new Artillery(ImageLoader.loadImage("/artwork/tanks/ArtilleryU.png"), new Rectangle(new Point((s.getWidthInTiles() - 15) * 32 - i * 32, (s.getHeightInTiles() - 1) * 32)), 2);
			Panzer g = new Goliath(ImageLoader.loadImage("/artwork/tanks/GoliathU.png"), new Rectangle(new Point((s.getWidthInTiles() - 11) * 32 - i * 9 * 32, (s.getHeightInTiles() - 1) * 32)), 2);
			l.setId(i + 6);
			a.setId(i + 8);
			g.setId(i + 10);
			g.setxOffset(s.position.x);
			g.setyOffset(s.position.y);
			a.setxOffset(s.position.x);
			a.setyOffset(s.position.y);
			l.setxOffset(s.position.x);
			l.setyOffset(s.position.y);
			g.setAction("moveTank");
			g.addClickedListener(inGamePanel);
			a.setAction("moveTank");
			a.addClickedListener(inGamePanel);
			l.setAction("moveTank");

			l.addClickedListener(inGamePanel);
			inGamePanel.addElement(g);
			inGamePanel.getPanzerPLayer2().add(g);
			
			inGamePanel.getPanzerPLayer2().add(l);
			inGamePanel.getPanzerPLayer2().add(a);
			
			inGamePanel.addElement(a);
			inGamePanel.addElement(l);
			if (connection != null) {// remove when network is done
				if (connection.isServer) {
					connection.ns.addRemoteableListener(l);
					connection.ns.addRemoteableListener(a);
					connection.ns.addRemoteableListener(g);
				} else {
					connection.nc.addRemoteableListener(l);
					connection.nc.addRemoteableListener(a);
					connection.nc.addRemoteableListener(g);
				}

			}
		}

		s.addClickedListener(inGamePanel);

		inGamePanel.addElement(s);

		Image bg = ImageLoader.loadImage("/artwork/bg.png");
		System.out.println("bg status: "+bg);
		ImageElement bgElement = new ImageElement(0, bg, 1);
		inGamePanel.addElement(bgElement);

		return inGamePanel;

	}

	public static GUIPanel DebugScreen(Game game) {

		SpielfeldFactory spFactory = new SpielfeldFactory();
		int[][] intField = spFactory.createForest(15, 30);

		Tile[][] tiles = new Tile[19][30];

		for (int i = 0; i < 19; i++) {
			for (int j = 0; j < 30; j++) {
				// Bodenteile
				if (intField[i][j] == 10) {// 10 Waldboden
					tiles[i][j] = new Tile(ImageLoader.loadImage("/artwork/tiles/Waldboden1.png"), 1, 0, 0);

				} 
				// Elemente
				else if (intField[i][j] == 1) {// Baumspitze TileID 1
					tiles[i][j] = new Tile(ImageLoader.loadImage("/artwork/tiles/BaumA.png"), 1, 1, 1);

				} 
				else if(intField[i][j]==2){// Busch TileID 2
					tiles[i][j] = new Tile(ImageLoader.loadImage("/artwork/tiles/WaldbodenStrauch.png"), 1, 2, 2);
				} else if (intField[i][j] == 4) {// Wasser TileID 3
					tiles[i][j] = new Tile(ImageLoader.loadImage("/artwork/tiles/Wasser2.png"), 1, 3, 3);
				} else if (intField[i][j] == 5) {// Stein TileID 4
					tiles[i][j] = new Tile(ImageLoader.loadImage("/artwork/tiles/WaldbodenStein.png"), 1, 2, 4);
				} else if (intField[i][j] == 8) {// Basis TileID 5
					tiles[i][j] = new Tile(ImageLoader.loadImage("/artwork/tiles/Waldboden1.png"), 1, 0, 5);
				} else if (intField[i][j] == 9) {// Baumstumpf TileID 6
					tiles[i][j] = new Tile(ImageLoader.loadImage("/artwork/tiles/BaumB.png"), 1, 2, 6);
				}

			}
		}

		Spielfeld s = new Spielfeld(new Rectangle(new Point()), 1, tiles);
		IngamePanel ret = new IngamePanel(s, game, null);

		for (int i = 0; i < 3; i++) {
			Panzer p = new Panzer(ImageLoader.loadImage("/artwork/tank.png"), new Rectangle(new Point((s.getWidthInTiles() - 1) * 32 - i * 32, (s.getHeightInTiles() - 1) * 32)), 2, 1, 1, 1, 1);
			p.setxOffset(s.position.x);
			p.setyOffset(s.position.y);
			p.setAction("moveTank");
			p.addClickedListener(ret);
			ret.addElement(p);
		}

		s.addClickedListener(ret);

		ret.addElement(s);

		Image bg = ImageLoader.loadImage("/artwork/bg.png");

		ImageElement bgElement = new ImageElement(0, bg, 1);
		ret.addElement(bgElement);

		return ret;
	}
	
	public static GUIPanel TutorialPanel(Game game) {
		TutorialPanel ret = new TutorialPanel();
		
		ImageElement backdrop = new ImageElement(1,ImageLoader.loadImage("/artwork/Helpscreen.png"),1);
		ImageElement goBack = new ImageElement(new Rectangle(0,Launcher.DRAWABLE_HEIGHT - 25,95,25),5,ImageLoader.loadImage("/artwork/GoBack.png"),1);
		goBack.setAction("goBackToMenu");
		goBack.addClickedListener(game);
		
		ret.addElement(backdrop);
		ret.addElement(goBack);
		
		return ret;
		
	}
	
	
}
