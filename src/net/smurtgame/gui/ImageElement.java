package net.smurtgame.gui;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;

public class ImageElement extends GUIElement {

	Image img;
	int scale;
	int xOffset;
	int yOffset;
	protected Image scaledImg;
	public ImageElement(Rectangle rect, int zIndex, Image img, int scale) {
		super(rect, zIndex);
		this.img = img;
		this.scale = scale;

	}
	
	public ImageElement(Rectangle rect, int zIndex, Image img, int scale, String action) {
		super(rect, zIndex,action);
		this.img = img;
		this.scale = scale;

	}
	
	public ImageElement(int zIndex, Image img, int scale) {
		super(new Rectangle(new Point()), zIndex);
		super.position.height = img.getHeight(null);
		super.position.width = img.getWidth(null);
		this.img = img;
		this.scale = scale;

	}
	
	public void render(Graphics2D g)
	{
		if(scaledImg == null)
			scaledImg = img.getScaledInstance(img.getWidth(null) * scale, img.getHeight(null) * scale,Image.SCALE_FAST);
		
		int x = super.position.x;
		int y = super.position.y;
		g.drawImage(scaledImg, x + xOffset,y + yOffset,null);
	}
	
	
	
	@Override
	public boolean isClicked(int clickX, int clickY) {
		
		if(!position.contains(clickX-xOffset, clickY-yOffset))
			return false;
		for(GUIElementClickedListener listener : super.listeners){
			listener.onClicked(this);
		}
		return true;
			
		
	}

	public int getxOffset() {
		return xOffset;
	}

	public void setxOffset(int xOffset) {
		this.xOffset = xOffset;
	}

	public int getyOffset() {
		return yOffset;
	}

	public void setyOffset(int yOffset) {
		this.yOffset = yOffset;
	}

}
