package net.smurtgame;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.ArrayList;

import net.smurtgame.gui.GUIElement;
import net.smurtgame.gui.GUIElementClickedListener;
import net.smurtgame.gui.Tile;

/**
 * Das Spielfeld beinhaltet Aktionen, welche zu den einzelnen Tiles selbst geh�ren. 
 * Besitzt ebenfalls Mehtoden, die Informationen �ber das SPielfeld selbst zur�ckgeben.
 */
public class Spielfeld extends GUIElement{

	private Tile[][] tilearr;


	private int heightInTiles;
	private int widthInTiles;

	//Gibt die H�he des Feldes in Tiles zur�ck (masseinheit, also nicht Pixel)
	public int getHeightInTiles() {
		return heightInTiles;
	}

	//Setzt die H�he in Tiles
	public void setHeightInTiles(int heightInTiles) {
		this.heightInTiles = heightInTiles;
	}

	//Gibt die Breite des Feldes in Tiles zur�ck
	public int getWidthInTiles() {
		return widthInTiles;
	}

	//Setzt die Breite in Tiles
	public void setWidthInTiles(int widthInTiles) {
		this.widthInTiles = widthInTiles;
	}
	
	public Tile[][] getTilearr() {
		return tilearr;
	}
	
	public void setTilearr(Tile[][] tilearr) {
		this.tilearr = tilearr;
	}

	//Konstruktor setzt Position, L�nge und Breite des Feldes in der Superklasse GUIElement
	public Spielfeld(Rectangle rect, int zPos, Tile[][] tiles) {
		super(rect, zPos);
		heightInTiles = tiles.length;
		widthInTiles = tiles[0].length;
		super.position.y = ((Launcher.DRAWABLE_HEIGHT - height())/2);
		super.position.x = ((Launcher.DRAWABLE_WIDTH - width())/2);
		super.position.width = width();
		super.position.height = height();
		
		System.out.println(super.position.x);
		
		setTiles(tiles);
	}

	//Die H�he des Spielfeldes in Pixel ist 32 * anzahl Tiles (1 Tile = 32 px)
	public int height(){
		return this.heightInTiles * 32;
	}
	
	//Die Breite des Spielfeldes in Pixel ist 32 * anzahl Tiles (1 Tile = 32 px)
	public int width(){
		return this.widthInTiles * 32;
	}


	private ArrayList<Tile> tiles;
	
	
	public void render(Graphics2D g)
	{
		for(int i = 0; i < tilearr.length; i++)
		{
			for(int j = 0; j < tilearr[i].length; j++){
				if(tilearr[i][j] != null)
					tilearr[i][j].render(g);
			}
		}
	}
	
	//Reiht das Spielfeld richtig aus (Align)
	public void adjustTilePos(Tile t, int i, int j)
	{
		t.setxOffset(super.position.x);
		t.setyOffset(super.position.y);
		
		Rectangle pos = new Rectangle();
		pos.height = t.getPos().height;
		pos.width = t.getPos().width;
		pos.x = j * 32;
		pos.y = i * 32;
		
		t.setPos(pos);
		
		
		
	}
	
	//Setzt alle Tiles in einem TileArray
	public void setTiles(Tile[][] tiles)
	{

		for(int i = 0; i < tiles.length; i++)
		{
			for(int j = 0; j < tiles[i].length; j++){
				if(tiles[i][j] != null)
					adjustTilePos(tiles[i][j],i,j);
			}
		}
		this.tilearr = tiles;
		
		
	}
	
	public ArrayList<Tile> getTilesList(){
		ArrayList<Tile> ret = new ArrayList<Tile>();
		for(int i = 0; i < this.tilearr.length; i++)
		{
			for(int j = 0; j < this.tilearr[i].length; j++){
				if(this.tilearr[i][j] != null)
					ret.add(this.tilearr[i][j]);
			}
		}
		
		return ret;
	}

	//F�gt jedem Feld einen ClickedListener hinzu
	@Override
	public void addClickedListener(GUIElementClickedListener listener) {
		for(int i = 0; i < tilearr.length; i++)
		{
			for(int j = 0; j < tilearr[i].length; j++){
				if(tilearr[i][j] != null)
				{					
					tilearr[i][j].addClickedListener(listener);
					tilearr[i][j].setAction("tileclick");
				}
					
			}
		}
	}

	//Pr�ft, welches Tile geklickt wird, anhand der Koordinaten
	@Override
	public boolean isClicked(int clickX, int clickY) {
		for(int i = 0; i < tilearr.length; i++)
		{
			for(int j = 0; j < tilearr[i].length; j++){
				if(tilearr[i][j] != null)
					if(tilearr[i][j].isClicked(clickX, clickY))
						return true;
			}
		}
		
		return false;
	}
	
	
	public void clear()
	{ 
		this.tiles = new ArrayList<Tile>();
		
	}
	
	
	
}

