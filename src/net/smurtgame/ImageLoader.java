package net.smurtgame;

import java.awt.Image;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;

/**
 * Der ImageLoader wird, wie der Name vermuten l�sst, beim laden eines Bildes aufgerufen.
 * Er findet den Pfad des Bildes heraus und gibt diesen via return zur�ck. 
 */
public class ImageLoader {

	private static HashMap<String, Image> loadedImg = new HashMap<String,Image>();
	
	//Die Methode, welche den Pfad eines Bildes nachverfolgt und zur�ckgibt.
	public static Image loadImage(String path){
		Image ret = null;
		
		if(loadedImg.containsKey(path)){
			return loadedImg.get(path);
		} else {
			try {
				loadedImg.put(path, ImageIO.read(ImageLoader.class.getResourceAsStream(path)));
				return loadImage(path);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return ret;
	}
	
	//Testmethode zur Z�hlung der geladenen Bilder.
	public static void debug()
	{
		System.out.println("ImageLoader has "+loadedImg.size()+" loaded images");
	}
}
