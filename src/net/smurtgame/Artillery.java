package net.smurtgame;

import java.awt.Image;
import java.awt.Rectangle;

/**
 * Diese Klasse beinhaltet den Panzertyp Artillerie
 * Die Artillerie besitzt die gr�sste Reichweite im Spiel, hat jedoch eine nicht sehr starke feuerkraft und kann sich schlecht bewegen. 
 * Superklasse dieser Klasse ist Panzer.
 */
public class Artillery extends Panzer {

	// Artillery, long range/medium strengh/low movement
	public Artillery(Image img, Rectangle pos, int z) {
		// defines the properties of the tank
		super(img, pos, z, 30, 250, 20, 80);
		resetMovePoints();

	}
	@Override
	public void resetMovePoints(){
		this.setMovePoints(1);
	}
}
