package net.smurtgame;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.event.MouseInputListener;

import net.smurtgame.gui.GUIElement;
import net.smurtgame.gui.GUIElementClickedListener;
import net.smurtgame.gui.GUIPanel;
import net.smurtgame.gui.GUIPanelFactory;
import net.smurtgame.gui.SearchPlayerPanel.PlayerSeeker;
import net.smurtgame.network.RemotePlayer;

/**
 * Game ist die Hauptklasse des Systems. 
 * Sie beinhaltet die Weiche, die zum In-Game-Bildschirm f�hrt und einige Interaktionen mit dem Netzwerk.
 */
public class Game implements MouseListener, KeyListener, GUIElementClickedListener, MouseInputListener {

	private int anzahlRunden;

	private Spielfeld spielfeld;
	private SoundManager soundManager;
	private Player yourPlayer;
	private RemotePlayer enemy;
	private String lel;

	private ArrayList<GUIPanel> menus = new ArrayList<GUIPanel>();
	private GUIPanel currentMenu;
	private boolean moveTank = false;

	private int mouseX;
	private int mouseY;

	//Im Konstruktor der Game klasse wird definiert, was gestartet wird.
	//Bei dieser Verzweigung wurden diverse Unit-Tests durchgef�hrt.
	public Game() {


		this.yourPlayer = new Player();
//		currentMenu = GUIPanelFactory.SearchPlayerScreen(this);

		//currentMenu = GUIPanelFactory.InGameScreen(this, null);


		//currentMenu = GUIPanelFactory.InGameScreen(this);

		currentMenu = GUIPanelFactory.TitleScreen(this);


		
	}

	//Gibt Spieler 2 zur�ck (Netzwerk)
	public RemotePlayer getEnemy(){
		return this.enemy;
	}
	
	//Gibt Spieler 1 zur�ck (Netzwerk)
	public Player getYou()
	{
		return yourPlayer;
	}
	
	//Referenziert auf die Klasse RemotePlayer im Package network.
	//Startet das Spiel mit dem Remote Gegner
	public void connectToPlayer(RemotePlayer player, PlayerSeeker connection){
		this.enemy = player;
		
		currentMenu = GUIPanelFactory.InGameScreen(this, connection);
	}
	
	//Setzt Name des Spielers 
	public void setName(String playerName){
		this.yourPlayer.setName(playerName);
	}

	//Tickt einmal im aktiven Menu.
	public void tick() {

		currentMenu.tick();
	}

	//Rendert das Aktuelle Menu
	public void render(Graphics2D g) {

		currentMenu.render(g);

	}

	//Diverse Actionlisteners
	@Override
	public void mouseClicked(MouseEvent e) {

		// currentMenu.clickedAt(e.getX(), e.getY());

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	//Wird bei Linksklick ausgef�hrt, erh�lt die aktuellen Koordinaten des Mauszeigers.
	@Override
	public void mousePressed(MouseEvent e) {
		if (e.getButton() == 1)
			currentMenu.clickedAt(e.getX(), e.getY());
		else

			currentMenu.rightClickedAt(e.getX(), e.getY());

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	//Wenn das GuiElement "searchPlayers" angeklickt wird, wird das aktuelle Menu ver�ndert.
	@Override
	public void onClicked(GUIElement guiElement) {
		 System.out.println("onClicked fired: "+guiElement.getAction());
		if (guiElement.getAction() == "multiplayer") {
			this.currentMenu = GUIPanelFactory.SearchPlayerScreen(this);
			//System.out.println("multiplayer not yet functional :(");
		}
		if(guiElement.getAction() == "singleplayer"){
			this.currentMenu = GUIPanelFactory.InGameScreen(this, null);
			yourPlayer.setName("Player 1");
			enemy = new RemotePlayer(null, "PLayer 2");
			enemy.setName("Player 2");
		}
		if(guiElement.getAction() == "tutorial")
		{
			this.currentMenu = GUIPanelFactory.TutorialPanel(this);
		}
		
		if(guiElement.getAction() == "goBackToMenu")
			
		{
			this.currentMenu = GUIPanelFactory.TitleScreen(this);
		}

	}

	//Wenn die Maus gedr�ckt bewegt wird, werden Koordinaten ausgelesen.
	@Override
	public void mouseDragged(MouseEvent arg0) {
		currentMenu.dragged(arg0.getX(), arg0.getY());

	}

	//Wenn die Maus bewegt wird, werden Koordinaten ausgelesen.
	@Override
	public void mouseMoved(MouseEvent arg0) {

		currentMenu.mouseX = arg0.getX();
		currentMenu.mouseY = arg0.getY();

	}

	@Override
	public void onRightClicked(GUIElement guiElement) {
		// TODO Auto-generated method stub
		
	}
	
	//Setzt das aktuelle Menu neu
	public void setMenu(GUIPanel menu)
	{
		this.currentMenu = menu;
	}

}
