package net.smurtgame;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import net.smurtgame.gui.ImageElement;
import net.smurtgame.network.Remoteable;
import net.smurtgame.network.packet.GameTankposPacket;
import net.smurtgame.network.packet.Packet;

/**
 * Superklasse aller Panzertypen (Artillery, Goliath und Leopard)
 * Beinhaltet die Aktionen der Panzer und was passiert, wenn sie angew�hlt werden usw. 
 */
public class Panzer extends ImageElement implements Remoteable {
	private Rectangle newPosition; // this is needed for moving or else you risk
	// triggering the isClicked event again. Go
	// to Fabian for in-depth explanation.
	private List<Rectangle> posibleTargets = new ArrayList<Rectangle>();
	private List<Rectangle> maximumRange = new ArrayList<Rectangle>();
	private List<Rectangle> posibleDirections = new ArrayList<Rectangle>();
	private int id;
	private int movePoints;
	private int health;
	private int strength;
	private int range;
	private int movement;
	protected Image img;
	int relativeDirection; // your opponent sees your tank other than you
	int realDirection; // this is the true direction
	private boolean renderMoveto = false;
	
	//getters and setters
	public Image getImg() {
		return img;
	}
	public void setImg(Image img) {
		this.img = img;
	}
	public List<Rectangle> getMaximumRange() {
		return maximumRange;
	}
	public void setMaximumRange(List<Rectangle> maximumRange) {
		this.maximumRange = maximumRange;
	}
	public int getRange() {
		return range;
	}
	public void setRange(int range) {
		this.range = range;
	}
	public List<Rectangle> getPosibleTargets() {
		return posibleTargets;
	}
	public void setPosibleTargets(List<Rectangle> posibleTargets) {
		this.posibleTargets = posibleTargets;
	}

	public int getHealth() {
		return health;
	}
	public void setHealth(int health) {
		this.health = health;
	}
	public int getMovePoints() {
		return movePoints;
	}
	public void setMovePoints(int movePoints) {
		this.movePoints = movePoints;
	}
	public int getMovement() {
		return movement;
	}
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setMovement(int movement) {
		this.movement = movement;
	}




	//Setzt fest, ob ein Panzer ein bestimmtes Feld begehen kann.
	public void setRenderMoveto(boolean renderMoveto) {
		this.renderMoveto = renderMoveto;
	}


	
	public void resetMovePoints(){
	}
	


	//Der Konstruktor setzt alle Eigenschaften des Panzers fest, die ihm von den drei Subklassen �bergeben werden.

	public Panzer(Image img, Rectangle pos, int z, int health, int strength, int movement, int range) {
		super(pos, z, img, Launcher.SCALE);
		pos.height = img.getHeight(null) * Launcher.SCALE;
		pos.width = img.getWidth(null) * Launcher.SCALE;
		this.health = health;
		this.strength = strength;
		this.movement = movement;
		this.range = range;
	}


	//Aktualisiert die Position des Panzers bei Ausf�hrung dieser tick() Methode.

	public void tick() {
		if (this.newPosition != null) {
			this.position = this.newPosition;
			this.newPosition = null;
		}

	}

	//Rendert von der Superklasse ImageElement den Panzer neu.
	public void render(Graphics2D g) {
		super.render(g);

		if (renderMoveto)
			renderMovetoGui(g);
	}

	//Bewegungsoptionen anzeigen.
	public void renderMovetoGui(Graphics2D g) {
		highlightActionOptions(g);
	}
	
	// highlights the possible move options for a tank 
	private void highlightActionOptions(Graphics2D g) {
		g.setColor(new Color(0, 255, 0, 50));
		for(Rectangle r:posibleDirections)
			g.fillRect(r.x, r.y, r.width, r.height);
		g.setColor(new Color(255, 255, 0,50));
		for(Rectangle r:maximumRange)
			g.fillRect(r.x, r.y, r.width, r.height);
		g.setColor(new Color(255, 0, 0,50));
		for(Rectangle r:posibleTargets)
			g.fillRect(r.x, r.y, r.width, r.height);
	}


	public void angreifen(Panzer tank){
		renderMoveto = false;
		tank.setHealth(tank.getHealth()-strength);
		System.out.println(tank.getId()+"wurde angriffen(YAY)");
		System.out.println(tank.getHealth());
		movePoints--;
		
	}

	//Bewegt den Panzer zu seiner neuen Position.

	public void movedTo(Rectangle newPos) {
		renderMoveto = false;
		this.newPosition = newPos;
		this.movePoints --;
	}

	//Wenn nicht bewegt wird oder nicht bewegt werden kann.
	public boolean moveCancelled() {
		return !renderMoveto;
	}

	//Wenn Panzer abgew�hlt wird.
	public void deselect() {
		renderMoveto = false;
	}

	//Wenn Panzer bewegt werden kann, werden die begehbaren Felder markiert.
	public void move() {

		renderMoveto = true;
	}

	//Liest alle m�glichen Bewegungsrichtungen des Panzers aus.
	public List<Rectangle> getPosibleDirections() {
		return posibleDirections;
	}
	
	//Setzt die m�glichen Bewegungsrichtungen neu.
	public void setPosibleDirections(List<Rectangle> posibleDirections) {
		this.posibleDirections = posibleDirections;
	}
	public Rectangle getNewPos()
	{
		return this.newPosition;
	}

	@Override
	public Packet toPacket() {
		//
		GameTankposPacket newPos = null;
		try {
			 newPos = new GameTankposPacket(this, InetAddress.getLocalHost());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newPos;
	}
	@Override
	public void onTankPosUpdated(GameTankposPacket packet) {
		System.out.println("received update for tank "+packet.getTankId()+". "+"I am tank "+this.id);
		if(this.id == packet.getTankId())
		{
			this.position.x = packet.getNewPos().x;
			this.position.y = packet.getNewPos().y;
		}
		
	}
}
