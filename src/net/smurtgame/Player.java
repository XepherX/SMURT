package net.smurtgame;

import java.util.ArrayList;

/**
 * Player beinhaltet die Aktionen, welche der Spieler ausf�hren kann.
 */
public class Player {
	
	private ArrayList<Panzer> panzerListe;
	private String name;
	
	//Wenn alle Panzer des Gegners zerst�rt wurden, ist das Spiel gewonnen.
	public void gewinnen(){
		
	}
	
	//Wenn eigene Panzer zerst�rt wurden, ist das Spiel verloren.
	public void verlieren(){
		
	}
	
	//Wenn Spieler aufgibt, ist das Spiel verloren.
	public void aufgebe(){
		
	}
	
	//Setzt Name des Spielers
	public void setName(String name){
		this.name = name;
	}
	
	//Liest Spielernamen aus.
	public String getName(){
		return this.name;
	}

}
