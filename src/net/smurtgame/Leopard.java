package net.smurtgame;

import java.awt.Image;
import java.awt.Rectangle;

/**
 * Diese Klasse beinhaltet den Panzertyp Leopard(Leichter Panzer)
 * Der Leopard besitzt eine hohe Reichweite und kann sich extrem weit bewegen. Jedoch hat er eine sehr schwache Feuerkraft. 
 * Superklasse dieser Klasse ist Panzer.
 */
public class Leopard extends Panzer{

	// light tank, high movment, low strength
	public Leopard (Image img, Rectangle pos, int z){
		// defines the properties of the tank
		super(img,pos, z, 100,20,5,5);
		resetMovePoints();
	}
	@Override
	public void resetMovePoints(){
		this.setMovePoints(1);
	}

}
